-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 07-Dez-2017 às 20:59
-- Versão do servidor: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `boteco`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

CREATE TABLE `cliente` (
  `cliente_id` int(11) NOT NULL,
  `nome` varchar(60) NOT NULL,
  `email` varchar(120) NOT NULL,
  `cpf` varchar(15) NOT NULL,
  `celular` varchar(15) NOT NULL,
  `pontos` int(4) NOT NULL,
  `ativo` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`cliente_id`, `nome`, `email`, `cpf`, `celular`, `pontos`, `ativo`) VALUES
(1, '-', 'cliente.default@oboteco.com', '000.000.000-00', '(00) 00000-0000', 0, 2),
(2, 'Roberto Justus', 'roberto.j@rederecord.com', '012.487.855-55', '(11) 98547-7444', 0, 1),
(3, 'Moacyr Franco', 'm.franco@gmail.com', '012.554.986-66', '(11) 98475-6522', 0, 1),
(4, 'Pedro de Lara', 'pedrodel@sbt.com.br', '021.548.885-44', '(58) 98547-5555', 0, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `compraproduto`
--

CREATE TABLE `compraproduto` (
  `compra_id` int(11) NOT NULL,
  `fornecedor_id` int(11) NOT NULL,
  `produto_id` int(11) NOT NULL,
  `valorCompra` double(9,2) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `dataCompra` date NOT NULL,
  `total` double(9,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `compraproduto`
--

INSERT INTO `compraproduto` (`compra_id`, `fornecedor_id`, `produto_id`, `valorCompra`, `quantidade`, `dataCompra`, `total`) VALUES
(1, 1, 1, 3.00, 50, '2017-12-07', 150.00);

-- --------------------------------------------------------

--
-- Estrutura da tabela `fornecedor`
--

CREATE TABLE `fornecedor` (
  `fornecedor_id` int(11) NOT NULL,
  `razaoSocial` varchar(120) NOT NULL,
  `cnpj` varchar(18) NOT NULL,
  `UF` varchar(2) NOT NULL,
  `email` varchar(60) NOT NULL,
  `celular` varchar(15) NOT NULL,
  `ativo` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `fornecedor`
--

INSERT INTO `fornecedor` (`fornecedor_id`, `razaoSocial`, `cnpj`, `UF`, `email`, `celular`, `ativo`) VALUES
(1, 'Ambev Bebidas Ltda.', '96.198.695/0001-99', 'RS', 'dpto.vendas@ambev.com', '(53) 98445-2014', 1),
(2, 'Gelei Bebidas Ltda.', '92.195.692/0001-99', 'RS', 'bebidasvendas@gelei.com.br', '(53) 98445-1045', 1),
(3, 'Atacadão da Bebida Ltda.', '01.254.888/4147-52', 'RO', 'vendas@atacadaobebidas.com.br', '(56) 41547-9511', 1),
(4, 'Cia. do salgadinho Ltda.', '95.158.698/0001-87', 'RS', 'salgadinhos@ciadosalgado.com.br', '(53) 98445-1000', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `funcionario`
--

CREATE TABLE `funcionario` (
  `funcionario_id` int(11) NOT NULL,
  `nome` varchar(60) NOT NULL,
  `email` varchar(80) NOT NULL,
  `cpf` varchar(15) NOT NULL,
  `senha` varchar(32) NOT NULL,
  `foto` varchar(50) NOT NULL,
  `celular` varchar(15) NOT NULL,
  `ativo` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `funcionario`
--

INSERT INTO `funcionario` (`funcionario_id`, `nome`, `email`, `cpf`, `senha`, `foto`, `celular`, `ativo`) VALUES
(1, 'Rafael Calearo', 'rafael.calearo@oboteco.com', '012.522.124-48', '9a707c30d406a223937e438263cb1606', '1.png', '(53) 98445-7380', 1),
(2, 'Igor Silva', 'igor.silva@oboteco.com', '015.145.189-47', 'f239c52daf2131fed80fdbfc8e991796', '', '(53) 98398-2578', 1),
(3, 'Thauã Borges', 'thaua.borges@oboteco.com', '014.256.147-14', 'ac1ce028f134b7902eb62d57d22b312b', '', '(53) 98398-2587', 1),
(4, 'Lussandro Marques', 'lussandro.marques@oboteco.com', '015.145.189-58', '381093ce4839fb5b921460702edd9056', '', '(53) 98358-2578', 1),
(5, 'Angelo Luz', 'angelo.luz@oboteco.com.br', '114.521.100-54', 'f199e15dd187b96c6ee8e2fbecca35a4', '-', '(53) 98445-1745', 0),
(6, 'Alex Bernardes ', 'alex.bernardes@oboteco.com.br', '012.574.145-47', '21e38f396e5bb1df01d86104e9b3ce27', '-', '(53) 98547-8957', 1),
(7, 'Reginaldo Rossi', 'reginaldo.rossi@oboteco.com.br', '011.254.587-47', 'c434787b68e8868f32d9b93057424c05', '-', '(53) 98445-2014', 0),
(8, 'Juliana Oliveira', 'juliana.oliveira@oboteco.com.br', '014.587.965-47', '16727dd8244f52c658437c3af61b897f', '-', '(53) 98145-1745', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `itenspedido`
--

CREATE TABLE `itenspedido` (
  `pedido_id` int(11) NOT NULL,
  `produto_id` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `valorVenda` double(9,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `itenspedido`
--

INSERT INTO `itenspedido` (`pedido_id`, `produto_id`, `quantidade`, `valorVenda`) VALUES
(1, 1, 3, 5.70);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pagamento`
--

CREATE TABLE `pagamento` (
  `pagamento_id` int(11) NOT NULL,
  `descricao` varchar(60) NOT NULL,
  `ativo` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `pagamento`
--

INSERT INTO `pagamento` (`pagamento_id`, `descricao`, `ativo`) VALUES
(1, 'Dinheiro', 1),
(2, 'Cartão Visa', 1),
(3, 'Cheque ', 0),
(4, 'Boleto', 1),
(5, 'Banrisul', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido`
--

CREATE TABLE `pedido` (
  `pedido_id` int(11) NOT NULL,
  `totalPedido` double(9,2) NOT NULL,
  `dataEmissao` date NOT NULL,
  `cliente_id` int(4) NOT NULL,
  `pagamento_id` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `pedido`
--

INSERT INTO `pedido` (`pedido_id`, `totalPedido`, `dataEmissao`, `cliente_id`, `pagamento_id`) VALUES
(1, 17.10, '2017-12-07', 1, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

CREATE TABLE `produto` (
  `produto_id` int(11) NOT NULL,
  `descricao` varchar(50) NOT NULL,
  `valorVenda` double(9,2) NOT NULL,
  `estoque` int(11) NOT NULL,
  `ativo` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `produto`
--

INSERT INTO `produto` (`produto_id`, `descricao`, `valorVenda`, `estoque`, `ativo`) VALUES
(1, 'Bohemia Long Neck 355ml', 5.70, 47, 1),
(2, 'Brahma Pilsen 600ml', 0.00, 0, 1),
(3, 'Kaiser Lager 600ml', 0.00, 0, 1),
(4, 'Sol Premium 330ml', 0.00, 0, 1),
(5, 'Coca-Cola 2L', 0.00, 0, 1),
(6, 'Pastelzinho 100kg', 0.00, 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`cliente_id`);

--
-- Indexes for table `compraproduto`
--
ALTER TABLE `compraproduto`
  ADD PRIMARY KEY (`compra_id`),
  ADD KEY `fornecedor_id` (`fornecedor_id`),
  ADD KEY `produto_id` (`produto_id`);

--
-- Indexes for table `fornecedor`
--
ALTER TABLE `fornecedor`
  ADD PRIMARY KEY (`fornecedor_id`);

--
-- Indexes for table `funcionario`
--
ALTER TABLE `funcionario`
  ADD PRIMARY KEY (`funcionario_id`);

--
-- Indexes for table `itenspedido`
--
ALTER TABLE `itenspedido`
  ADD KEY `pedido_id` (`pedido_id`),
  ADD KEY `produto_id` (`produto_id`);

--
-- Indexes for table `pagamento`
--
ALTER TABLE `pagamento`
  ADD PRIMARY KEY (`pagamento_id`);

--
-- Indexes for table `pedido`
--
ALTER TABLE `pedido`
  ADD PRIMARY KEY (`pedido_id`),
  ADD KEY `cliente_id` (`cliente_id`),
  ADD KEY `produto_id` (`pagamento_id`),
  ADD KEY `cliente_id_2` (`cliente_id`),
  ADD KEY `pagamento_id` (`pagamento_id`);

--
-- Indexes for table `produto`
--
ALTER TABLE `produto`
  ADD PRIMARY KEY (`produto_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cliente`
--
ALTER TABLE `cliente`
  MODIFY `cliente_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `compraproduto`
--
ALTER TABLE `compraproduto`
  MODIFY `compra_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fornecedor`
--
ALTER TABLE `fornecedor`
  MODIFY `fornecedor_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `funcionario`
--
ALTER TABLE `funcionario`
  MODIFY `funcionario_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `pagamento`
--
ALTER TABLE `pagamento`
  MODIFY `pagamento_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pedido`
--
ALTER TABLE `pedido`
  MODIFY `pedido_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `produto`
--
ALTER TABLE `produto`
  MODIFY `produto_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `compraproduto`
--
ALTER TABLE `compraproduto`
  ADD CONSTRAINT `compraproduto_ibfk_1` FOREIGN KEY (`fornecedor_id`) REFERENCES `fornecedor` (`fornecedor_id`),
  ADD CONSTRAINT `compraproduto_ibfk_2` FOREIGN KEY (`produto_id`) REFERENCES `produto` (`produto_id`);

--
-- Limitadores para a tabela `itenspedido`
--
ALTER TABLE `itenspedido`
  ADD CONSTRAINT `itenspedido_ibfk_1` FOREIGN KEY (`produto_id`) REFERENCES `produto` (`produto_id`),
  ADD CONSTRAINT `itenspedido_ibfk_3` FOREIGN KEY (`pedido_id`) REFERENCES `pedido` (`pedido_id`);

--
-- Limitadores para a tabela `pedido`
--
ALTER TABLE `pedido`
  ADD CONSTRAINT `pedido_ibfk_1` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`cliente_id`),
  ADD CONSTRAINT `pedido_ibfk_2` FOREIGN KEY (`pagamento_id`) REFERENCES `pagamento` (`pagamento_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
