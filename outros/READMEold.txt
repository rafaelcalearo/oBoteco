# O Boteco

O "projetoBoteco" é um trabalho (Aula 10) para ser desenvolvido em aula/casa
que faz parte da avaliação da disciplina de Algoritmos e Programação II do segundo 
semestre da Faculdade de Tecnologia Senac Pelotas–RS e pertence ao grupo: 
[Igor Silva](https://gitlab.com/Maartyr), [Thauã Borges](https://gitlab.com/thaua97), 
[Lussandro Marques](https://gitlab.com/lmarques) e [Rafael Calearo](https://gitlab.com/rafaelcalearo) (criador do repositório). 

## O Projeto

Bom, o projeto está sendo desenvolvido na linguagem de programação **Java** e 
conta com as seguintes tecnologias para o seu desenvolvimento: 
**Java Swing (API)**, **IDE NetBeans 8.2**, **Sublime Text 2**, **MySQL (BD)**…

## Objetivo

O objetivo então é implementar um CRUD simples que atenda alguns requisitos 
(usar estritamente a linguagem de programação Java, criar o projeto com o 
**[Maven](https://maven.apache.org/)**, escolher um programa fictício e etc.) e que englobe conceitos 
aprendidos no semestre. Mais informação do enunciado do exercício [aqui]
(https://www.dropbox.com/s/u6sby35m07pbhez/2017-10-05%20-%20Aula%2010%20-%20Algoritmos%20II%20-%20Trabalho%20Integrador.pdf?dl=0).

## Diagrama ER

Nosso diagrama foi desenvolvido usando a ferramenta **MySQL Workbench 6.3 CE** 
que possibilita ter uma visão de todas as tabelas que vão fazer parte do *software*:

* [DER - versão 1](https://image.ibb.co/erjiy6/ER_Boteco.png);
* DER - versão atual:

![DER Atual](https://image.ibb.co/d71yMR/DER_Rafael_Img_v_2.png)

## Requisitos do Sistema

Os requisitos em um *software* são as descrições das funções e restrições que o 
programa/*software* a ser desenvolvido deve possuir. Conforme o 
[enunciado do exercício](https://www.dropbox.com/s/u6sby35m07pbhez/2017-10-05%20-%20Aula%2010%20-%20Algoritmos%20II%20-%20Trabalho%20Integrador.pdf?dl=0) 
o grupo deveria simular uma **entrevista** ou 
outro **método** da **[Engenharia de Software](https://pt.wikipedia.org/wiki/Engenharia_de_software)** 
para o levantamento desses requisitos (funcionais e não funcionais). Logo temos 
os requisitos listados de uma **cervejaria fictícia**:

### Requisitos Funcionais

* **[RF 001]** O sistema deve permitir a inclusão, alteração e ativação/desativação de fornecedores;
* **[RF 002]** O sistema deve permitir a inclusão, alteração e ativação/desativação de clientes;
* **[RF 003]** O sistema deve permitir a inclusão, alteração e ativação/desativação de produtos;
* **[RF 004]** O sistema deve permitir a inclusão de pedidos;
* **[RF 005]** O programa permite a listagem de vendas e controle de estoque;

### Requisitos Não Funcionais

* **[RNF 001]** O sistema deve rodar em plataformas Windows ou Linux;
* **[RNF 002]** O sistema deve controlar o estoque quando for cadastrado o produto e a movimentação dos pedidos;
* **[RNF 003]** O Banco de Dados será o MySQL; 
* **[RNF 004]** Linguagem Java;