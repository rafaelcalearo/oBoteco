package local.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import local.beans.Fornecedor;
import local.utilitarios.Conexao;

/**
 *
 * @author Rafael
 */
public class FornecedorDAO {

    /**
     * METODO Q LISTA OS FORNECEDORES CADASTRADOS NO BANCO DE DADOS
     *
     * @return UMA LISTA DESSES FORNECEDORES P/ SEREM MOSTRADOS NA TELA
     */
    public List<Fornecedor> getLista() {
        String sql = "SELECT * FROM fornecedor WHERE ativo = 1";
        List<Fornecedor> lista = new ArrayList<>();
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Fornecedor fornecedor = new Fornecedor();
                fornecedor.setFornecedor_id(rs.getInt("fornecedor_id"));
                fornecedor.setRazaoSocial(rs.getString("razaoSocial"));
                fornecedor.setCNPJ(rs.getString("cnpj"));
                fornecedor.setUF(rs.getString("UF"));
                fornecedor.setEmail(rs.getString("email"));
                fornecedor.setCelular(rs.getString("celular"));
                lista.add(fornecedor);
            }
            pst.close();
            rs.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
        }
        return lista;
    }

    /**
     * METODO Q LISTA OS FORNECEDORES DESATIVADOS (A ONDE A FLAG ativo É IGUAL A
     * "0") DO BANCO DE DADOS
     *
     * @return UMA LISTA DESSES FORNECEDORES DESATIVADOS P/ SEREM MOSTRADOS NA
     * TELA
     */
    public List<Fornecedor> getListaDesativados() {
        String sql = "SELECT * FROM fornecedor WHERE ativo = 0";
        List<Fornecedor> lista = new ArrayList<>();
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Fornecedor fornecedor = new Fornecedor();
                fornecedor.setFornecedor_id(rs.getInt("fornecedor_id"));
                fornecedor.setRazaoSocial(rs.getString("razaoSocial"));
                fornecedor.setCNPJ(rs.getString("cnpj"));
                fornecedor.setUF(rs.getString("UF"));
                fornecedor.setEmail(rs.getString("email"));
                fornecedor.setCelular(rs.getString("celular"));
                lista.add(fornecedor);
            }
            pst.close();
            rs.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
        }
        return lista;
    }

    /**
     * METODO Q SERVE P/ SALVAR OU ALTERAR UM OBJETO DO TIPO FORNECEDOR, CASO
     * ESSE ESTEJA C/ O ID VAZIO QUER DIZER Q É P/ CADASTRAR O MESMO NO BD, OU
     * SEJA A RECEM ESTA SENDO CRIADO ESSE OBJETO
     *
     * @param fornecedor É UM OBJETO A SER CADASTRADO OU ALTERADO
     * @return É UMA MENSAGEM (STRING) DE CONFIRMAÇÃO OU N D INCLUSAO/ALTERACAO
     */
    public int salvar(Fornecedor fornecedor) {
        if (fornecedor.getFornecedor_id() == null) {
            return incluir(fornecedor);
        } else {
            return alterar(fornecedor);
        }
    }

    /**
     * METODO Q INCLUI UM FORNECEDOR NO BANCO DE DADOS
     *
     * @param fornecedor É UM OBJETO A SER INCLUIDO NO BANCO DE DADOS
     * @return É UMA MENSAGEM DE CONFIRMACAO OU NAO DA INCLUSAO
     */
    public int incluir(Fornecedor fornecedor) {
        String sql = "INSERT INTO fornecedor (razaoSocial, cnpj, UF, email, "
                + "celular, ativo) VALUES(?,?,?,?,?,?)";
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            pst.setString(1, fornecedor.getRazaoSocial());
            pst.setString(2, fornecedor.getCNPJ());
            pst.setString(3, fornecedor.getUF());
            pst.setString(4, fornecedor.getEmail());
            pst.setString(5, fornecedor.getCelular());
            pst.setInt(6, fornecedor.getAtivo());
            if (pst.executeUpdate() > 0) {
                pst.close();
                return 1;
            } else {
                pst.close();
                return 2;
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
            return 0;
        }
    }

    /**
     * METODO Q ALTERA UM FORNECEDOR CADASTRADO NO BANCO DE DADOS
     *
     * @param fornecedor É UM OBJETO A SER ALTERADO NO BANCO DE DADOS
     * @return É UMA MENSAGEM DE CONFIRMACAO OU NAO DA ALTERACAO
     */
    public int alterar(Fornecedor fornecedor) {
        String sql = "UPDATE fornecedor SET razaoSocial = ?, cnpj = ?, UF = ?, "
                + "email = ?, celular = ?, ativo = ? WHERE fornecedor_id = ?";
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            pst.setString(1, fornecedor.getRazaoSocial());
            pst.setString(2, fornecedor.getCNPJ());
            pst.setString(3, fornecedor.getUF());
            pst.setString(4, fornecedor.getEmail());
            pst.setString(5, fornecedor.getCelular());
            pst.setInt(6, fornecedor.getAtivo());
            pst.setInt(7, fornecedor.getFornecedor_id());
            if (pst.executeUpdate() > 0) {
                pst.close();
                return 3;
            } else {
                pst.close();
                return 4;
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
            return 0;
        }
    }

    /**
     * METODO Q SERVER P/ LOCALIZAR UM FORNECEDOR NO BANCO DE DADOS
     *
     * @param id É O ID DO FORNECEDOR A SER LOCALIZADO
     * @return É O FORNECEDOR CONTENDO OS DADOS OU NULO CASO N SEJA ENCONTRADO O
     * FORNECEDOR CUJO O ID FOI PASSADO
     */
    public Fornecedor localizar(Integer id) {
        String sql = "SELECT * FROM fornecedor WHERE fornecedor_id = ?";
        Fornecedor fornecedor = new Fornecedor();
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            pst.setInt(1, id);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                fornecedor.setFornecedor_id(rs.getInt("fornecedor_id"));
                fornecedor.setRazaoSocial(rs.getString("razaoSocial"));
                fornecedor.setCNPJ(rs.getString("cnpj"));
                fornecedor.setUF(rs.getString("UF"));
                fornecedor.setEmail(rs.getString("email"));
                fornecedor.setCelular(rs.getString("celular"));
                return fornecedor;
            }
            pst.close();
            rs.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
        }
        return null;
    }

    /**
     * METODO Q SERVER P/ LOCALIZAR UM FORNECEDOR NO BANCO DE DADOS
     *
     * @param razaoSocial É A RAZAO SOCIAL DO FORNECEDOR A SER LOCALIZADA
     * @return É O FORNECEDOR CONTENDO OS DADOS OU NULO CASO N SEJA ENCONTRADO O
     * FORNECEDOR CUJA A RAZAO SOCIAL FOI PASSADA
     */
    public Fornecedor localizar(String razaoSocial) {
        String sql = "SELECT * FROM fornecedor WHERE razaoSocial = ?";
        Fornecedor fornecedor = new Fornecedor();
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            pst.setString(1, razaoSocial);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                fornecedor.setFornecedor_id(rs.getInt("fornecedor_id"));
                fornecedor.setRazaoSocial(rs.getString("razaoSocial"));
                fornecedor.setCNPJ(rs.getString("cnpj"));
                fornecedor.setUF(rs.getString("UF"));
                fornecedor.setEmail(rs.getString("email"));
                fornecedor.setCelular(rs.getString("celular"));
                return fornecedor;
            }
            pst.close();
            rs.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
        }
        return null;
    }
}
