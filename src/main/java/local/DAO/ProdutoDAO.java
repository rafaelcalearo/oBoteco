package local.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import local.beans.Produto;
import local.utilitarios.Conexao;

/**
 *
 * @author Rafael
 */
public class ProdutoDAO {

    /**
     * METODO Q LISTA OS PRODUTOS CADASTRADOS NO BANCO DE DADOS
     *
     * @return UMA LISTA DESSES PRODUTOS P/ SEREM MOSTRADOS NA TELA
     */
    public List<Produto> getLista() {
        String sql = "SELECT * FROM produto WHERE ativo = 1";
        List<Produto> lista = new ArrayList<>();
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Produto produto = new Produto();
                produto.setProduto_id(rs.getInt("produto_id"));
                produto.setDescricao(rs.getString("descricao"));
                produto.setValorVenda(rs.getDouble("valorVenda"));
                produto.setEstoque(rs.getInt("estoque"));
                produto.setAtivo(rs.getInt("ativo"));
                lista.add(produto);
            }
            pst.close();
            rs.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
        }
        return lista;
    }

    /**
     * METODO Q LISTA OS PRODUTOS DESATIVADOS (A ONDE A FLAG ativo É IGUAL A
     * "0") DO BANCO DE DADOS
     *
     * @return UMA LISTA DESSES PRODUTOS DESATIVADOS P/ SEREM MOSTRADOS NA TELA
     */
    public List<Produto> getListaDesativados() {
        String sql = "SELECT * FROM produto WHERE ativo = 0";
        List<Produto> lista = new ArrayList<>();
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Produto produto = new Produto();
                produto.setProduto_id(rs.getInt("produto_id"));
                produto.setDescricao(rs.getString("descricao"));
                produto.setValorVenda(rs.getDouble("valorVenda"));
                produto.setEstoque(rs.getInt("estoque"));
                produto.setAtivo(rs.getInt("ativo"));
                lista.add(produto);
            }
            pst.close();
            rs.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
        }
        return lista;
    }

    /**
     * METODO Q SERVE P/ SALVAR OU ALTERAR UM OBJETO DO TIPO PRODUTO, CASO ESSE
     * ESTEJA C/ O ID VAZIO QUER DIZER Q É P/ CADASTRAR O MESMO NO BD, OU SEJA A
     * RECEM ESTA SENDO CRIADO ESSE OBJETO
     *
     * @param produto É UM OBJETO A SER CADASTRADO OU ALTERADO
     * @return É UMA MENSAGEM (STRING) DE CONFIRMAÇÃO OU N D INCLUSAO/ALTERACAO
     */
    public int salvar(Produto produto) {
        if (produto.getProduto_id() == null) {
            return incluir(produto);
        } else {
            return alterar(produto);
        }
    }

    /**
     * METODO Q INCLUI UM PRODUTO NO BANCO DE DADOS
     *
     * @param produto É UM OBJETO A SER INCLUIDO NO BANCO DE DADOS
     * @return É UMA MENSAGEM DE CONFIRMACAO OU NAO DA INCLUSAO
     */
    public int incluir(Produto produto) {
        String sql = "INSERT INTO produto (descricao, valorVenda, estoque, "
                + " ativo) VALUES(?,?,?,?)";
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            pst.setString(1, produto.getDescricao());
            pst.setDouble(2, produto.getValorVenda());
            pst.setInt(3, produto.getEstoque());
            pst.setInt(4, produto.getAtivo());
            if (pst.executeUpdate() > 0) {
                pst.close();
                return 1;
            } else {
                pst.close();
                return 2;
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
            return 0;
        }
    }

    /**
     * METODO Q ALTERA UM PRODUTO CADASTRADO NO BANCO DE DADOS
     *
     * @param produto É UM OBJETO A SER ALTERADO NO BANCO DE DADOS
     * @return É UMA MENSAGEM DE CONFIRMACAO OU NAO DA ALTERACAO
     */
    public int alterar(Produto produto) {
        String sql = "UPDATE produto SET descricao = ? WHERE produto_id = ?";
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            pst.setString(1, produto.getDescricao());
            pst.setInt(2, produto.getProduto_id());
            if (pst.executeUpdate() > 0) {
                pst.close();
                return 3;
            } else {
                pst.close();
                return 4;
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
            return 0;
        }
    }

    /**
     * METODO Q SERVER P/ LOCALIZAR UM PRODUTO NO BANCO DE DADOS
     *
     * @param id É O ID DO PRODUTO A SER LOCALIZADO
     * @return É O PRODUTO CONTENDO OS DADOS OU NULO CASO N SEJA ENCONTRADO O
     * PRODUTO CUJO O ID FOI PASSADO
     */
    public Produto localizar(Integer id) {
        String sql = "SELECT * FROM produto WHERE produto_id = ?";
        Produto produto = new Produto();
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            pst.setInt(1, id);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                produto.setProduto_id(rs.getInt("produto_id"));
                produto.setDescricao(rs.getString("descricao"));
                produto.setValorVenda(rs.getDouble("valorVenda"));
                produto.setEstoque(rs.getInt("estoque"));
                produto.setAtivo(rs.getInt("ativo"));
                return produto;
            }
            pst.close();
            rs.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
        }
        return null;
    }

    /**
     * METODO Q SERVER P/ LOCALIZAR UM PRODUTO NO BANCO DE DADOS
     *
     * @param descricao É A DESCRICAO DO PRODUTO A SER LOCALIZADA
     * @return É O PRODUTO CONTENDO OS DADOS OU NULO CASO N SEJA ENCONTRADO O
     * PRODUTO CUJO A DESCRICAO FOI PASSADA
     */
    public Produto localizar(String descricao) {
        String sql = "SELECT * FROM produto WHERE descricao = ?";
        Produto produto = new Produto();
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            pst.setString(1, descricao);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                produto.setProduto_id(rs.getInt("produto_id"));
                produto.setDescricao(rs.getString("descricao"));
                produto.setValorVenda(rs.getDouble("valorVenda"));
                produto.setEstoque(rs.getInt("estoque"));
                produto.setAtivo(rs.getInt("ativo"));
                return produto;
            }
            pst.close();
            rs.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
        }
        return null;
    }

    /**
     * METODO Q BUSCA A QUANTIDADE DO PRODUTO NO ESTOQUE
     *
     * @param id É O ID DO PRODUTO CUJA A QUANTIDADE DEVE SER INFORMADA
     * @return É A QUANTIDADE NO ESTOQUE DESSE PRODUTO PESQUISADO NO BD
     */
    public int buscaQuantidadeEstoque(int id) {
        String sql = "SELECT estoque FROM produto WHERE produto_id = ?";
        PreparedStatement pst = new Conexao().getPreparedStatement(sql);
        try {
            pst.setInt(1, id);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                return rs.getInt("estoque");
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
        }
        return 0;
    }

    /**
     * METODO Q SERVE P/ ATUALIZAR AS INFORMACOES PASSADAS P/ TAL PRODUTO
     *
     * @param id É O ID DO RRODUTO A SER ATUALIZADO
     * @param valor É O VALOR EM REAIS A SER ATUALIZADO DO PRODUTO
     * @param quantidade É A QUANTIDADE (ESTOQUE) A SER ATUALIZADA DO PRODUTO
     * @return TRUE (SIM) SE ATUALIZOU C/ SUCESSO OU FALSE (NAO) S N ATUALIZOU
     */
    public boolean atualizaProduto(int id, double valor, int quantidade) {
        String sql = "UPDATE produto SET valorVenda = ?, estoque = ? WHERE "
                + "produto_id = ?";
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            pst.setDouble(1, (valor + (valor * 90) / 100));
            pst.setInt(2, (buscaQuantidadeEstoque(id) + quantidade));
            pst.setInt(3, id);
            return pst.executeUpdate() > 0;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
            return false;
        }
    }
        
    public boolean diminuiQuantidadeProduto(int id, int quantidade) {
        String sql = "UPDATE produto SET estoque = ? WHERE produto_id = ?";
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);           
            pst.setInt(1, (buscaQuantidadeEstoque(id) - quantidade));
            pst.setInt(2, id);
            return pst.executeUpdate() > 0;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
            return false;
        }
    }

    public Produto buscaItem(String pesquisa) {
        String sql = "SELECT * FROM produto WHERE descricao LIKE '%" + pesquisa + "%' ";
        Produto produto = new Produto();
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                produto.setProduto_id(rs.getInt("produto_id"));
                produto.setDescricao(rs.getString("descricao"));
                produto.setValorVenda(rs.getDouble("valorVenda"));
                produto.setEstoque(rs.getInt("estoque"));
                produto.setAtivo(rs.getInt("ativo"));
                return produto;
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
        }
        return null;
    }

}
