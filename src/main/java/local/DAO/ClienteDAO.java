package local.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import local.beans.Cliente;
import local.utilitarios.Conexao;

/**
 *
 * @author Rafael
 */
public class ClienteDAO {

    /**
     * METODO Q LISTA OS CLIENTES CADASTRADOS NO BANCO DE DADOS
     *
     * @return UMA LISTA DESSES CLIENTES P/ SEREM MOSTRADOS NA TELA
     */
    public List<Cliente> getLista() {
        String sql = "SELECT * FROM cliente WHERE ativo = 1";
        List<Cliente> lista = new ArrayList<>();
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Cliente cliente = new Cliente();
                cliente.setCliente_id(rs.getInt("cliente_id"));
                cliente.setNome(rs.getString("nome"));
                cliente.setEmail(rs.getString("email"));
                cliente.setCPF(rs.getString("cpf"));
                cliente.setCelular(rs.getString("celular"));
                cliente.setPontos(rs.getInt("pontos"));
                lista.add(cliente);
            }
            rs.close();
            pst.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
        }
        return lista;
    }

    /**
     * METODO Q LISTA OS CLIENTES DESATIVADOS (A ONDE A FLAG ativo É IGUAL A
     * "0") DO BANCO DE DADOS
     *
     * @return UMA LISTA DESSES FUNCIONARIOS DESATIVADOS P/ SEREM MOSTRADOS NA
     * TELA
     */
    public List<Cliente> getListaDesativados() {
        String sql = "SELECT * FROM cliente WHERE ativo = 0";
        List<Cliente> lista = new ArrayList<>();
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Cliente cliente = new Cliente();
                cliente.setCliente_id(rs.getInt("cliente_id"));
                cliente.setNome(rs.getString("nome"));
                cliente.setEmail(rs.getString("email"));
                cliente.setCPF(rs.getString("cpf"));
                cliente.setCelular(rs.getString("celular"));
                cliente.setPontos(rs.getInt("pontos"));
                lista.add(cliente);
            }
            rs.close();
            pst.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
        }
        return lista;
    }

    /**
     * METODO Q SERVE P/ SALVAR OU ALTERAR UM OBJETO DO TIPO CLIENTE, CASO ESSE
     * ESTEJA C/ O ID VAZIO QUER DIZER Q É P/ CADASTRAR O MESMO NO BD, OU SEJA A
     * RECEM ESTA SENDO CRIADO ESSE OBJETO
     *
     * @param cliente É UM OBJETO A SER CADASTRADO OU ALTERADO
     * @return É UMA MENSAGEM (STRING) DE CONFIRMAÇÃO OU N D INCLUSAO/ALTERACAO
     */
    public int salvar(Cliente cliente) {
        if (cliente.getCliente_id() == null) {
            return incluir(cliente);
        } else {
            return alterar(cliente);
        }
    }

    /**
     * METODO Q INCLUI UM CLIENTE NO BANCO DE DADOS
     *
     * @param cliente É UM OBJETO A SER INCLUIDO NO BANCO DE DADOS
     * @return É UMA MENSAGEM DE CONFIRMACAO OU NAO DA INCLUSAO
     */
    public int incluir(Cliente cliente) {
        String sql = "INSERT INTO cliente (nome, email, cpf, celular, "
                + "pontos, ativo) VALUES(?,?,?,?,?,?)";
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            pst.setString(1, cliente.getNome());
            pst.setString(2, cliente.getEmail());
            pst.setString(3, cliente.getCPF());
            pst.setString(4, cliente.getCelular());
            pst.setInt(5, cliente.getPontos());
            pst.setInt(6, cliente.getAtivo());
            if (pst.executeUpdate() > 0) {
                pst.close();
                return 1;
            } else {
                pst.close();
                return 2;
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
            return 0;
        }
    }

    /**
     * METODO Q ALTERA UM CLIENTE CADASTRADO NO BANCO DE DADOS
     *
     * @param cliente É UM OBJETO A SER ALTERADO NO BANCO DE DADOS
     * @return É UMA MENSAGEM DE CONFIRMACAO OU NAO DA ALTERACAO
     */
    public int alterar(Cliente cliente) {
        String sql = "UPDATE cliente SET nome = ?, email = ?, cpf = ?, "
                + "celular = ?, pontos = ?, ativo = ? WHERE cliente_id = ?";
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            pst.setString(1, cliente.getNome());
            pst.setString(2, cliente.getEmail());
            pst.setString(3, cliente.getCPF());
            pst.setString(4, cliente.getCelular());
            pst.setInt(5, cliente.getPontos());
            pst.setInt(6, cliente.getAtivo());
            pst.setInt(7, cliente.getCliente_id());
            if (pst.executeUpdate() > 0) {
                pst.close();
                return 3;
            } else {
                pst.close();
                return 4;
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
            return 0;
        }
    }

    /**
     * METODO Q SERVER P/ LOCALIZAR UM CLIENTE NO BANCO DE DADOS
     *
     * @param id É O ID DO CLIENTE A SER LOCALIZADO
     * @return É O CLIENTE CONTENDO OS DADOS OU NULO CASO N SEJA ENCONTRADO O
     * CLIENTE CUJO O ID FOI PASSADO
     */
    public Cliente localizar(Integer id) {
        String sql = "SELECT * FROM cliente WHERE cliente_id = ?";
        Cliente cliente = new Cliente();
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            pst.setInt(1, id);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                cliente.setCliente_id(rs.getInt("cliente_id"));
                cliente.setNome(rs.getString("nome"));
                cliente.setEmail(rs.getString("email"));
                cliente.setCPF(rs.getString("cpf"));
                cliente.setCelular(rs.getString("celular"));
                cliente.setPontos(rs.getInt("pontos"));
                cliente.setAtivo(rs.getInt("ativo"));
                return cliente;
            }
            pst.close();
            rs.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
        }
        return null;
    }

    /**
     * METODO Q SERVER P/ LOCALIZAR UM CLIENTE NO BANCO DE DADOS PELO NOME
     *
     * @param nome É O NOME DO CLIENTE A SER LOCALIZADO
     * @return É O CLIENTE CONTENDO OS DADOS OU NULO CASO N SEJA ENCONTRADO O
     * CLIENTE CUJO O ID FOI PASSADO
     */
    public Cliente localizar(String nome) {
        String sql = "SELECT * FROM cliente WHERE nome = ?";
        Cliente cliente = new Cliente();
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            pst.setString(1, nome);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                cliente.setCliente_id(rs.getInt("cliente_id"));
                cliente.setNome(rs.getString("nome"));
                cliente.setEmail(rs.getString("email"));
                cliente.setCPF(rs.getString("cpf"));
                cliente.setCelular(rs.getString("celular"));
                cliente.setPontos(rs.getInt("pontos"));
                cliente.setAtivo(rs.getInt("ativo"));
                return cliente;
            }
            pst.close();
            rs.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
        }
        return null;
    }

    public String buscaCliente(String pesquisa) {
        String sql = "SELECT * FROM cliente WHERE nome LIKE '%" + pesquisa + "%' ";
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {               
                return rs.getString("nome");
            } else {
                pst.close();
                return "";
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
            return "";
        }
    }
}
