package local.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import local.beans.Pagamento;
import local.utilitarios.Conexao;

/**
 *
 * @author Rafael
 */
public class PagamentoDAO {

    /**
     * METODO Q LISTA AS FORMAS DE PAGAMENTO CADASTRADAS NO BANCO DE DADOS
     *
     * @return É UMA LISTA DESSAS FORMAS DE PAG. P/ SEREM MOSTRADOS NA TELA
     */
    public List<Pagamento> getLista() {
        String sql = "SELECT * FROM pagamento WHERE ativo = 1";
        List<Pagamento> lista = new ArrayList<>();
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Pagamento pagamento = new Pagamento();
                pagamento.setPagamento_id(rs.getInt("pagamento_id"));
                pagamento.setDescricao(rs.getString("descricao"));
                pagamento.setAtivo(rs.getInt("ativo"));
                lista.add(pagamento);
            }
            pst.close();
            rs.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
        }
        return lista;
    }

    /**
     * METODO Q LISTA AS FORMAS DE PAGAMENTO DESATIVADAS (A ONDE A FLAG ativo É
     * IGUAL A "0") DO BANCO DE DADOS
     *
     * @return UMA LISTA DESSAS FORMAS DE PAGAMENTO DESATIVADOS P/ SEREM
     * MOSTRADOS NA TELA
     */
    public List<Pagamento> getListaDesativados() {
        String sql = "SELECT * FROM pagamento WHERE ativo = 0";
        List<Pagamento> lista = new ArrayList<>();
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Pagamento pagamento = new Pagamento();
                pagamento.setPagamento_id(rs.getInt("pagamento_id"));
                pagamento.setDescricao(rs.getString("descricao"));
                pagamento.setAtivo(rs.getInt("ativo"));
                lista.add(pagamento);
            }
            pst.close();
            rs.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
        }
        return lista;
    }

    /**
     * METODO Q SERVE P/ SALVAR OU ALTERAR UM OBJETO DO TIPO PAGAMENTO, CASO
     * ESSE ESTEJA C/ O ID VAZIO QUER DIZER Q É P/ CADASTRAR O MESMO NO BD, OU
     * SEJA A RECEM ESTA SENDO CRIADO ESSE OBJETO
     *
     * @param pagamento É UM OBJETO A SER CADASTRADO OU ALTERADO
     * @return É UMA MENSAGEM (STRING) DE CONFIRMAÇÃO OU N D INCLUSAO/ALTERACAO
     */
    public int salvar(Pagamento pagamento) {
        if (pagamento.getPagamento_id() == null) {
            return incluir(pagamento);
        } else {
            return alterar(pagamento);
        }
    }

    /**
     * METODO Q INCLUI UMA FORMA DE PAGAMENTO NO BANCO DE DADOS
     *
     * @param pagamento É UM OBJETO A SER INCLUIDO NO BANCO DE DADOS
     * @return É UMA MENSAGEM DE CONFIRMACAO OU NAO DA INCLUSAO
     */
    public int incluir(Pagamento pagamento) {
        String sql = "INSERT INTO pagamento (descricao, ativo) VALUES(?,?)";
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            pst.setString(1, pagamento.getDescricao());
            pst.setInt(2, pagamento.getAtivo());
            if (pst.executeUpdate() > 0) {
                pst.close();
                return 1;
            } else {
                pst.close();
                return 2;
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
            return 0;
        }
    }

    /**
     * METODO Q ALTERA UMA FORMA DE PAGAMENTO CADASTRADO NO BANCO DE DADOS
     *
     * @param pagamento É UM OBJETO A SER ALTERADO NO BANCO DE DADOS
     * @return É UMA MENSAGEM DE CONFIRMACAO OU NAO DA ALTERACAO
     */
    public int alterar(Pagamento pagamento) {
        String sql = "UPDATE pagamento SET descricao = ?, ativo = ? WHERE pagamento_id = ?";
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            pst.setString(1, pagamento.getDescricao());
            pst.setInt(2, pagamento.getAtivo());
            pst.setInt(3, pagamento.getPagamento_id());
            if (pst.executeUpdate() > 0) {
                pst.close();
                return 3;
            } else {
                pst.close();
                return 4;
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
            return 0;
        }
    }

    /**
     * METODO Q SERVER P/ LOCALIZAR UMA FORMA DE PAGAMENTO NO BANCO DE DADOS
     *
     * @param id É O ID DA FORMA DE PAGAMENTO A SER LOCALIZADA
     * @return É A FORMA DE PAGAMENTO CONTENDO OS DADOS OU NULO CASO N SEJA
     * ENCONTRADO A FORMA DE PAGAMENTO CUJO O ID FOI PASSADO
     */
    public Pagamento localizar(Integer id) {
        String sql = "SELECT * FROM pagamento WHERE pagamento_id = ?";
        Pagamento pagamento = new Pagamento();
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            pst.setInt(1, id);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                pagamento.setPagamento_id(rs.getInt("pagamento_id"));
                pagamento.setDescricao(rs.getString("descricao"));
                pagamento.setAtivo(rs.getInt("ativo"));
                return pagamento;
            }
            pst.close();
            rs.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
        }
        return null;
    }
    
    /**
     * METODO Q SERVER P/ LOCALIZAR UMA FORMA DE PAGAMENTO NO BANCO DE DADOS
     *
     * @param descricao É O ID DA FORMA DE PAGAMENTO A SER LOCALIZADA
     * @return É A FORMA DE PAGAMENTO CONTENDO OS DADOS OU NULO CASO N SEJA
     * ENCONTRADO A FORMA DE PAGAMENTO CUJO O ID FOI PASSADO
     */
    public Pagamento localizar(String descricao) {
        String sql = "SELECT * FROM pagamento WHERE descricao = ?";
        Pagamento pagamento = new Pagamento();
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            pst.setString(1, descricao);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                pagamento.setPagamento_id(rs.getInt("pagamento_id"));
                pagamento.setDescricao(rs.getString("descricao"));
                pagamento.setAtivo(rs.getInt("ativo"));
                return pagamento;
            }
            pst.close();
            rs.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
        }
        return null;
    }
}
