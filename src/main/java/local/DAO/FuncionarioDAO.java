package local.outros;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import local.beans.Funcionario;
import local.utilitarios.Conexao;

/**
 *
 * @author Rafael
 */
public class FuncionarioDAO {

    /**
     * METODO Q LISTA OS FUNCIONARIOS CADASTRADOS NO BANCO DE DADOS
     *
     * @return UMA LISTA DESSES FUNCIONARIOS P/ SEREM MOSTRADOS NA TELA
     */
    public List<Funcionario> getLista() {
        String sql = "SELECT * FROM funcionario WHERE ativo = 1";
        List<Funcionario> lista = new ArrayList<>();
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Funcionario funcionario = new Funcionario();
                funcionario.setFuncionario_id(rs.getInt("funcionario_id"));
                funcionario.setNome(rs.getString("nome"));
                funcionario.setEmail(rs.getString("email"));
                funcionario.setCPF(rs.getString("cpf"));
                funcionario.setSenha(rs.getString("senha"));
                funcionario.setFoto(rs.getString("foto"));
                funcionario.setCelular(rs.getString("celular"));
                funcionario.setAtivo(rs.getInt("ativo"));
                lista.add(funcionario);
            }
            rs.close();
            pst.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
        }
        return lista;
    }

    /**
     * METODO Q LISTA OS FUNCIONARIOS DESATIVADOS (A ONDE A FLAG ativo É IGUAL A
     * "0") DO BANCO DE DADOS
     *
     * @return UMA LISTA DESSES FUNCIONARIOS DESATIVADOS P/ SEREM MOSTRADOS NA
     * TELA
     */
    public List<Funcionario> getListaDesativados() {
        String sql = "SELECT * FROM funcionario WHERE ativo = 0";
        List<Funcionario> lista = new ArrayList<>();
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Funcionario funcionario = new Funcionario();
                funcionario.setFuncionario_id(rs.getInt("funcionario_id"));
                funcionario.setNome(rs.getString("nome"));
                funcionario.setEmail(rs.getString("email"));
                funcionario.setCPF(rs.getString("cpf"));
                funcionario.setSenha(rs.getString("senha"));
                funcionario.setFoto(rs.getString("foto"));
                funcionario.setCelular(rs.getString("celular"));
                funcionario.setAtivo(rs.getInt("ativo"));
                lista.add(funcionario);
            }
            rs.close();
            pst.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
        }
        return lista;
    }

    /**
     * METODO Q SERVE P/ SALVAR OU ALTERAR UM OBJETO DO TIPO FUNCIONARIO, CASO
     * ESSE ESTEJA C/ O ID VAZIO QUER DIZER Q É P/ CADASTRAR O MESMO NO BD, OU
     * SEJA A RECEM ESTA SENDO CRIADO ESSE OBJETO
     *
     * @param funcionario É UM OBJETO A SER CADASTRADO OU ALTERADO
     * @return É UMA MENSAGEM (STRING) DE CONFIRMAÇÃO OU N D INCLUSAO/ALTERACAO
     */
    public int salvar(Funcionario funcionario) {
        if (funcionario.getFuncionario_id() == null) {
            return incluir(funcionario);
        } else {
            return alterar(funcionario);
        }
    }

    /**
     * METODO Q INCLUI UM FUNCIONARIO NO BANCO DE DADOS
     *
     * @param funcionario É UM OBJETO A SER INCLUIDO NO BANCO DE DADOS
     * @return É UMA MENSAGEM DE CONFIRMACAO OU NAO DA INCLUSAO
     */
    public int incluir(Funcionario funcionario) {
        String sql = "INSERT INTO funcionario (nome, email, cpf, senha, "
                + "foto, celular, ativo) VALUES(?,?,?,?,?,?,?)";
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            pst.setString(1, funcionario.getNome());
            pst.setString(2, funcionario.getEmail());
            pst.setString(3, funcionario.getCPF());
            pst.setString(4, funcionario.getSenha());
            pst.setString(5, funcionario.getFoto());
            pst.setString(6, funcionario.getCelular());
            pst.setInt(7, funcionario.getAtivo());
            if (pst.executeUpdate() > 0) {
                pst.close();
                return 1;
            } else {
                pst.close();
                return 2;
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
            return 0;
        }
    }

    /**
     * METODO Q ALTERA UM FUNCIONARIO CADASTRADO NO BANCO DE DADOS
     *
     * @param funcionario É UM OBJETO A SER ALTERADO NO BANCO DE DADOS
     * @return É UMA MENSAGEM DE CONFIRMACAO OU NAO DA ALTERACAO
     */
    public int alterar(Funcionario funcionario) {
        String sql = "UPDATE funcionario SET nome = ?, email = ?, cpf = ?, "
                + "senha = ?, foto = ?, celular = ?, ativo = ? WHERE funcionario_id = ?";
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            pst.setString(1, funcionario.getNome());
            pst.setString(2, funcionario.getEmail());
            pst.setString(3, funcionario.getCPF());
            pst.setString(4, funcionario.getSenha());
            pst.setString(5, funcionario.getFoto());
            pst.setString(6, funcionario.getCelular());
            pst.setInt(7, funcionario.getAtivo());
            pst.setInt(8, funcionario.getFuncionario_id());
            if (pst.executeUpdate() > 0) {
                pst.close();
                return 3;
            } else {
                pst.close();
                return 4;
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
            return 0;
        }
    }

    /**
     * METODO Q SERVER P/ LOCALIZAR UM FUNCIONARIO NO BANCO DE DADOS
     *
     * @param id É O ID DO FUNCIONARIO A SER LOCALIZADO
     * @return É O FUNCIONARIO CONTENDO OS DADOS OU NULO CASO N SEJA ENCONTRADO
     * O FUNCIONARIO CUJO O ID FOI PASSADO
     */
    public Funcionario localizar(Integer id) {
        String sql = "SELECT * FROM funcionario WHERE funcionario_id = ?";
        Funcionario funcionario = new Funcionario();
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            pst.setInt(1, id);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                funcionario.setFuncionario_id(rs.getInt("funcionario_id"));
                funcionario.setNome(rs.getString("nome"));
                funcionario.setEmail(rs.getString("email"));
                funcionario.setCPF(rs.getString("cpf"));
                funcionario.setSenha(rs.getString("senha"));
                funcionario.setFoto(rs.getString("foto"));
                funcionario.setCelular(rs.getString("celular"));
                funcionario.setAtivo(rs.getInt("ativo"));
                return funcionario;
            }
            pst.close();
            rs.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
        }
        return null;
    }

    /**
     * METODO Q VALIDA O ACESSO DO USUÁRIO NO SISTEMA
     *
     * @param login ESPERA-SE A PRIMEIRA PARTE DE UM E-MAIL EX.: rafael.calearo
     * @param senha ESPERA-SE UMA SENHA
     * @return É UMA STRING VAZIA CASO O FUNCIONARIO N SEJA ENCONTRADO COM AS
     * CREDENCIAS PASSADAS (login e senha) NO BD, OU O NOME CASO ENCONTRADO
     */
    public String validaLogin(String login, String senha) {
        String sql = "SELECT nome FROM funcionario WHERE email = ? AND senha = ? AND ativo = 1";
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            pst.setString(1, login + "@oboteco.com");
            pst.setString(2, senha);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                return rs.getString("nome");
            }
            pst.close();
            rs.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
        }
        return "";
    }
}
