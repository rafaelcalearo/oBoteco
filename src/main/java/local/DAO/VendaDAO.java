package local.DAO;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import local.beans.Compra;
import local.beans.Venda;
import local.utilitarios.Conexao;

/**
 *
 * @author Rafael
 */
public class VendaDAO {

    //VARIAVEIS DE INSTANCIA
    PedidoDAO pedidoDAO;
    ProdutoDAO produtoDAO;

    //CONSTRUTOR 
    public VendaDAO() {
        pedidoDAO = new PedidoDAO();
        produtoDAO = new ProdutoDAO();
    }

    /**
     * METODO Q LISTA AS COMPRAS CADASTRADAS NO BANCO DE DADOS
     *
     * @return UMA LISTA DESSAS COMPRAS P/ SEREM MOSTRADAS NA TELA
     */
    public List<Venda> getLista() {
        String sql = "SELECT * FROM itenspedido";
        List<Venda> lista = new ArrayList<>();
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Venda venda = new Venda();
                venda.setPedido(pedidoDAO.localizar(rs.getInt("pedido_id")));
                venda.setProduto(produtoDAO.localizar(rs.getInt("produto_id")));
                venda.setQuantidade(rs.getInt("quantidade"));
                venda.setValorVenda(rs.getDouble("valorVenda"));
                lista.add(venda);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
        }
        return lista;
    }

    /**
     * METODO Q SERVE P/ SALVAR OU ALTERAR UMA COMPRA DO TIPO COMPRA, CASO ESSA
     * ESTEJA C/ O ID VAZIO QUER DIZER Q É P/ CADASTRAR ESSA COMPRA NO BD, OU
     * SEJA A RECEM ESTA SENDO CRIADO ESSE OBJETO
     *
     * @param compra É UM OBJETO A SER CADASTRADO OU ALTERADO
     * @return É UMA MENSAGEM (STRING) DE CONFIRMAÇÃO OU N D INCLUSAO/ALTERACAO
     */
//    public String salvar(Venda venda) {
//        if (venda.getCompra_id() == null) {
//            return incluir(compra);
//        } else {
//            return alterar(compra);
//        }
//    }
    /**
     * METODO Q INCLUI UMA COMPRA NO BANCO DE DADOS
     *
     * @param venda É UM OBJETO A SER INCLUIDO NO BANCO DE DADOS
     */
    public void incluir(Venda venda) {
        String sql = "INSERT INTO itenspedido (pedido_id, produto_id, "
                + "quantidade, valorVenda) VALUES (?,?,?,?)";
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            pst.setInt(1, venda.getPedido().getPedido_id());
            pst.setInt(2, venda.getProduto().getProduto_id());
            pst.setInt(3, venda.getQuantidade());
            pst.setDouble(4, venda.getValorVenda());
            if (pst.executeUpdate() > 0
                    && produtoDAO.diminuiQuantidadeProduto(venda.getProduto().getProduto_id(), venda.getQuantidade())) {
                pst.close();
            } else {
                pst.close();
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
        }
    }

    /**
     * METODO Q ALTERA UMA COMPRA JA CADASTRADA NO BANCO DE DADOS
     *
     * @param compra É UM OBJETO A SER ALTERADO NO BANCO DE DADOS
     * @return É UMA MENSAGEM DE CONFIRMACAO OU NAO DA ALTERACAO
     */
    public String alterar(Compra compra) {
        String sql = "UPDATE compraproduto SET fornecedor_id = ?, produto_id = ?, "
                + "valorCompra = ?, quantidade = ?, dataCompra = ?, total = ? WHERE compra_id = ?";
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            pst.setInt(1, compra.getFornecedor().getFornecedor_id());
            pst.setInt(2, compra.getProduto().getProduto_id());
            pst.setDouble(3, compra.getValorCompra());
            pst.setInt(4, compra.getQuantidade());
            pst.setDate(5, Date.valueOf(compra.getDataCompra()));
            pst.setDouble(6, compra.getTotal());
            pst.setInt(7, compra.getCompra_id());
            if (pst.executeUpdate() > 0
                    && produtoDAO.atualizaProduto(compra.getProduto().getProduto_id(),
                            compra.getValorCompra(), compra.getQuantidade())) {
                pst.close();
                return "Alteração dos dados da compra realizados com sucesso!";
            } else {
                pst.close();
                return "Alteração dos dados da compra não realizados com sucesso!";
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
            return "";
        }
    }
}
