package local.DAO;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import local.beans.Pedido;
import local.utilitarios.Conexao;

/**
 *
 * @author Rafael
 */
public class PedidoDAO {

    //VARIAVEIS DE INSTANCIA
    ClienteDAO clienteDAO;
    PagamentoDAO pagamentoDAO;

    //CONSTRUTOR 
    public PedidoDAO() {
        clienteDAO = new ClienteDAO();
        pagamentoDAO = new PagamentoDAO();
    }

    /**
     * METODO Q LISTA AS COMPRAS CADASTRADAS NO BANCO DE DADOS
     *
     * @return UMA LISTA DESSAS COMPRAS P/ SEREM MOSTRADAS NA TELA
     */
    public List<Pedido> getLista() {
        String sql = "SELECT * FROM pedido";
        List<Pedido> lista = new ArrayList<>();
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Pedido pedido = new Pedido();
                pedido.setPedido_id(rs.getInt("pedido_id"));
                pedido.setTotalPedido(rs.getDouble("totalPedido"));
                pedido.setDataEmissao(rs.getDate("dataEmissao").toLocalDate());
                pedido.setCliente(clienteDAO.localizar(rs.getInt("cliente_id")));
                pedido.setPagamento(pagamentoDAO.localizar(rs.getInt("pagamento_id")));
                lista.add(pedido);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
        }
        return lista;
    }

    /**
     * METODO Q SERVE P/ SALVAR OU ALTERAR UMA COMPRA DO TIPO COMPRA, CASO ESSA
     * ESTEJA C/ O ID VAZIO QUER DIZER Q É P/ CADASTRAR ESSA COMPRA NO BD, OU
     * SEJA A RECEM ESTA SENDO CRIADO ESSE OBJETO
     *
     * @param pedido É UM OBJETO A SER CADASTRADO OU ALTERADO
     * @return É UMA MENSAGEM (STRING) DE CONFIRMAÇÃO OU N D INCLUSAO/ALTERACAO
     */
    public String salvar(Pedido pedido) {
        if (pedido.getPedido_id() == null) {
            return incluir(pedido);
        } else {
            return "";
        }
    }

    /**
     * METODO Q INCLUI UMA COMPRA NO BANCO DE DADOS
     *
     * @param pedido É UM OBJETO A SER INCLUIDO NO BANCO DE DADOS
     * @return É UMA MENSAGEM DE CONFIRMACAO OU NAO DA INCLUSAO
     */
    public String incluir(Pedido pedido) {
        String sql = "INSERT INTO pedido (totalPedido, dataEmissao, "
                + "cliente_id, pagamento_id) VALUES(?,?,?,?)";
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            pst.setDouble(1, pedido.getTotalPedido());
            pst.setDate(2, Date.valueOf(pedido.getDataEmissao()));
            pst.setInt(3, pedido.getCliente().getCliente_id());
            pst.setInt(4, pedido.getPagamento().getPagamento_id());
            if (pst.executeUpdate() > 0) {
                pst.close();
                return "Pedido realizado com sucesso!";
            } else {
                pst.close();
                return "Pedido não realizado com sucesso!";
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
            return "";
        }
    }
  
     public Pedido localizar(Integer id) {
        String sql = "SELECT pedido_id FROM pedido WHERE pedido_id = ?";
        Pedido pedido = new Pedido();
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            pst.setInt(1, id);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                pedido.setPedido_id(rs.getInt("pedido_id"));                
                return pedido;
            }
            pst.close();
            rs.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
        }
        return null;
    }
    
    public int proximoPedido() {
        String sql = "SELECT * FROM pedido ORDER BY pedido_id DESC LIMIT 1";
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                return rs.getInt("pedido_id") + 1;
            } else {
                return 1;
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
            return 0;
        }
    }
}
