package local.controller;

import local.DAO.CompraDAO;
import local.beans.Compra;

/**
 *
 * @author Rafael
 */
public class CompraController {

    CompraDAO compraDAO;

    public CompraController() {
        compraDAO = new CompraDAO();
    }

    public String verificaCamposFormulario(Compra compra) {
        if(compra.getFornecedor().getFornecedor_id() < 0){
            return "Selecione um fornecedor!";
        }
        if(compra.getProduto().getProduto_id() < 0){
            return "Selecione um produto!";
        }
        return "";
    }
}
