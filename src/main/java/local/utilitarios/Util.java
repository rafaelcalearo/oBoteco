package local.utilitarios;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 *
 * @author Rafael
 */
public class Util {

    public static LocalDate dateSQLParaLocalDate(Date data) {
        Instant instant = Instant.ofEpochMilli(data.getTime());
        return LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate();
    }

    public static Date localDateParaDate(LocalDate data) {
        return (Date) Date.from(data.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static String localDateAtualString() {
        LocalDate dataAgora = LocalDate.now();
        DateTimeFormatter formatador = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return (String) dataAgora.format(formatador);
    }

    public static String dateAtualString() {
        Date dataSistema = new Date();
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        return (String) formato.format(dataSistema);
    }
    
    public static LocalDate stringParaLocalDate(String d){
        DateTimeFormatter formatacaoData = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        String data = (String) d;
        return LocalDate.parse(data, formatacaoData);
    }
}
