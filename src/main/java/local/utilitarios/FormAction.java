package local.utilitarios;

/**
 *
 * @author Rafael
 */
public interface FormAction {
 
     void setIcone();
     
     void atualizaTabela();
     
     void desabelitaBotoes(boolean editando);
     
     public boolean validaCampos();
    
}
