package local.utilitarios;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import javax.swing.JOptionPane;

/**
 * CLASSE IMPOSSIVEL DE SER ESTENDIDA, PELA RAZÃO DE POSSUIR A PALAVRA-CHAVE
 * RESERVADA PELO JAVA final
 *
 * @author Rafael
 */
public final class Conexao {

    //URL É O END. DO BD, APOS O NOME DO BANCO "boteco" VEM CONFIG. ESPECÍFICAS
    private final String URL = "jdbc:mysql://localhost:3306/boteco?useTimezone=true&serverTimezone=UTC";
    //COMO O PROPRIO NOME DA VARIÁVEL DIZ, É O DRIVE DO MYSQL Q FOI ADD. C/ MAVEN
    private final String driver = "com.mysql.jdbc.Driver";
    private final String usuario = "root"; //NOME DO USUARIO Q ACESSA O BD
    private final String senha = ""; //SENHA DO BD

    public Conexao() { //CONSTRUTOR PADRAO
    }

    /**
     * METODO QUE CRIA A CONEXAO COM O BD
     *
     * @return UMA CONEXAO
     */
    public Connection getConexao() {

        Connection con = null;

        try {
            Class.forName(driver);
            con = DriverManager.getConnection(URL, usuario, senha);
        } catch (ClassNotFoundException | SQLException ex) {
            JOptionPane.showMessageDialog(null, ex, "Erro", 0);
        }
        return con;
    }

    /**
     * CASO CHAMEM ESSE METODO PRIMEIRO ELE VERIFICA SE HA CONEXAO!!! SE EXISTE
     * ENTAO UMA CONEXAO ELE EXECUTA O COMANDO SQL POIS EXISTE A CONEXAO
     *
     * @param sql RECEBE UMA INSTRUÇAO SQL
     * @return É A CONEXAO JUNTO COM A INSTRUCAO, SE NAO RETORNA NULO
     */
    public PreparedStatement getPreparedStatement(String sql) {
        try {
            return getConexao().prepareStatement(sql);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex, "Erro", 0);
        }
        return null;
    }
}
