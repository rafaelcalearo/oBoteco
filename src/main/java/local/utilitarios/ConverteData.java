package local.utilitarios;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import org.jdesktop.beansbinding.Converter;

/**
 *
 * @author Rafael
 */
public class ConverteData extends Converter {

    DateTimeFormatter formatacaoData = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    /**
     * ESSE METODO SERVE P/ LISTAR AS DATAS VINDAS DO BD PASSANDO POR UMA BEANS
     * ESSE METODO RECEBE UM OBJETO C/ UMA DATA DO TIPO LOCALDATE
     *
     * @param s PODE SER QLQUER TIPO DE OBJETO: COMPRA, FUNCIONÁRIO E ETC. Q
     * PRECISE CONVERTER DATAS
     * @return UMA DATA DO TIPO STRING PARA COLOCAR NA TABELA-VIWER (TELA)
     */
    @Override
    public Object convertForward(Object s) {
        LocalDate ld = (LocalDate) s;
        return formatacaoData.format(ld);        
    }

    /**
     * ESSE METODO RECEBE UM OBJETO C/ UMA DATA DO TIPO STRING, OU SEJA DA TELA,
     * MAIS ESPECIFICAMENTE DO CAMPO (FORMULÁRIO) Q PODE SER DO TIPO: JTEXTFILD
     *
     * @param t PODE SER QLQUER TIPO DE OBJETO: COMPRA, FUNCIONÁRIO E ETC. Q
     * PRECISE CONVERTER DATAS
     * @return UMA DATA DO TIPO LOCALDATE PARA INSERIR/ATUALIZAR NO BD
     */
    @Override
    public Object convertReverse(Object t) {
        String data = (String) t;
        LocalDate ld = LocalDate.parse(data, formatacaoData);
        return ld;
    }
}
