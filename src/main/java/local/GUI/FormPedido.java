package local.GUI;

import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import local.DAO.ClienteDAO;
import local.DAO.PagamentoDAO;
import local.DAO.PedidoDAO;
import local.DAO.ProdutoDAO;
import local.DAO.VendaDAO;
import local.beans.Pagamento;
import local.beans.Pedido;
import local.beans.Produto;
import local.beans.Venda;
import local.utilitarios.Util;

/**
 *
 * @author Rafael
 */
public class FormPedido extends javax.swing.JDialog {

    //DECLAÇÃO DE VARIÁVEIS
    private final PedidoDAO pedidoDAO;
    private final ClienteDAO clienteDAO;
    private final PagamentoDAO pagamentoDAO;
    private final ProdutoDAO produtoDAO;
    private final VendaDAO vendaDAO;
    private final DefaultTableModel modelo;
    private Produto produto;
    private double total;
    private int quantidade;
    private final List<Venda> listaAuxiliarItens;

    /**
     * Creates new form FormFornecedores
     *
     * @param parent
     * @param modal
     */
    public FormPedido(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setIcone();
        pedidoDAO = new PedidoDAO();
        clienteDAO = new ClienteDAO();
        pagamentoDAO = new PagamentoDAO();
        produtoDAO = new ProdutoDAO();
        vendaDAO = new VendaDAO();
        iniciaComponentes();
        desabelitaCamposBotoes(false);
        modelo = (DefaultTableModel) tblItens.getModel();
        produto = new Produto();
        listaAuxiliarItens = new ArrayList<>();
    }

    //METODO Q SERVE P/ COLOCAR/SETAR O ICONE (LOGO) N BARRA DE TITULO DA TELA
    private void setIcone() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/imagens/icone.png")));
    }

    /**
     * METODO Q SERVE P/ INICIAR CAMPOS INPUTS COMO POR EXEMPLO O DO "ID"
     */
    public final void iniciaComponentes() {
        txtId.setText("" + pedidoDAO.proximoPedido());
        txtData.setText(Util.localDateAtualString());
        listaPagamentos.addAll(pagamentoDAO.getLista());
        for (int registro = 0; registro < listaPagamentos.size(); registro++) {
            cbxPagamento.addItem(listaPagamentos.get(registro));
        }
        txtValorTotal.setText("0,00");
    }

    /**
     * METODO Q HABILITA/DESABILITA CAMPOS/BOTOES NA CRIACAO/EDICAO DE REGISTRO
     *
     * @param editando RECEBE TRUE (EDITANDO OU CRIANDO REGISTRO) OU FALSE
     */
    public final void desabelitaCamposBotoes(boolean editando) {
        txtBuscaCliente.setEditable(editando);
        cbxPagamento.setEnabled(editando);
        txtBuscaItem.setEditable(editando);
        txtQuantidade.setEditable(editando);
        btnOK.setEnabled(editando);
        btnNovo.setEnabled(!editando);
        btnSalvar.setEnabled(editando);
        btnCancelar.setEnabled(editando);
        btnLimpar.setEnabled(editando);
        btnFechar.setEnabled(!editando);
    }

    /**
     * METODO Q SERVE P/ MOSTRAR MENSAGENS NA TELA VINDAS DA CONTROLLER/DAO
     *
     * @param msg RECEBE UMA MENSAGEM P/ SER MOSTRADA NA TELA
     */
    public void mostraMensagens(String msg) {
        JOptionPane.showMessageDialog(this, msg);
    }

    /**
     * METODO Q SERVE P/ LIMPAR OS CAMPOS (INPUTS) DA TELA (FORMULÁRIO)
     */
    public final void limpaCampos() {
        txtBuscaCliente.setText("");
        txtCliente.setText("");
        txtBuscaItem.setText("");
        txtItem.setText("");
        txtQuantidade.setText("");
        while (modelo.getRowCount() > 0 && listaAuxiliarItens.size() > 0) {
            int i = 0;
            modelo.removeRow(i);
            listaAuxiliarItens.remove(i);
        }
        txtValorTotal.setText("0,00");
    }

    /**
     * METODO Q SERVE P/ CALCULAR O TOTAL DOS INTENS ADICIONADOS NO PEDIDO
     *
     * @param valor É O VALOR DO PRODUTO ADICIONADO AO PEDIDO
     * @return O TOTAL DE COMPRAS, OU SEJA, O CALCULO TOTAL DE INTES ADICIONADO
     */
    public final double calculaTotal(double valor) {
        return total += valor;
    }

    /**
     * METODO Q SERVE P/ VALIDAR OS CAMPOS (INPUTS) DA TELA (FORMULÁRIO)
     *
     * @return true (SIM) TODOS OS CAMPOS PREENCHIDOS CORRETAMENTE OU false
     * (NAO) ALGUM/TODOS OS CAMPOS C/ PROBLEMA
     */
    public final boolean validaCampos() {
        if (txtCliente.getText().trim().isEmpty()) {
            mostraMensagens("Inclua um cliente para efetuar o pedido!");
            txtBuscaCliente.requestFocus();
            return false;
        }
        if (modelo.getRowCount() == 0) {
            mostraMensagens("Tabela vazia, inclua um item precionando o botão \"OK\"!");
            return false;
        }
        validaBotaoOK();
        return true;
    }

    /**
     * METODO Q SERVER P/ VALIDAR O BOTAO "OK" AO SER CLICADO
     *
     * @return true (SIM) TODOS OS CAMPOS PREENCHIDOS CORRETAMENTE OU false
     * (NAO) ALGUM/TODOS OS CAMPOS C/ PROBLEMA
     */
    public final boolean validaBotaoOK() {
        if (txtItem.getText().trim().isEmpty()) {
            mostraMensagens("Inclua pelo menos um item para efetuar o pedido!");
            txtBuscaItem.requestFocus();
            return false;
        }
        if (!(txtQuantidade.getText().matches("\\d+"))) {
            mostraMensagens("Inclua a quantidade de produtos para efetuar o pedido!");
            txtQuantidade.requestFocus();
            return false;
        }
        return true;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        listaPagamentos = org.jdesktop.observablecollections.ObservableCollections.observableList(new ArrayList<Pagamento>())
        ;
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPListaFornecedor = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        txtId = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtData = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        cbxPagamento = new javax.swing.JComboBox<>();
        txtBuscaCliente = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtCliente = new javax.swing.JTextField();
        txtItem = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        btnOK = new javax.swing.JButton();
        txtBuscaItem = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txtQuantidade = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblItens = new javax.swing.JTable();
        jLabel7 = new javax.swing.JLabel();
        txtValorTotal = new javax.swing.JLabel();
        jPanelNavegacao = new javax.swing.JPanel();
        btnNovo = new javax.swing.JButton();
        btnSalvar = new javax.swing.JButton();
        btnFechar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnLimpar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("O Boteco - Realizar Pedido");

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/lupa.png"))); // NOI18N

        txtId.setEditable(false);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Id:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Data:");

        txtData.setEditable(false);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Buscar cliente:");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Forma de Pagamento:");

        txtBuscaCliente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscaClienteKeyReleased(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Cliente:");

        txtCliente.setEditable(false);

        txtItem.setEditable(false);

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setText("Item:");

        btnOK.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnOK.setText("OK");
        btnOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOKActionPerformed(evt);
            }
        });

        txtBuscaItem.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscaItemKeyReleased(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel9.setText("Quantidade:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtBuscaItem)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtItem, javax.swing.GroupLayout.PREFERRED_SIZE, 254, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnOK))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel2)
                            .addComponent(jLabel6)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtData, javax.swing.GroupLayout.PREFERRED_SIZE, 233, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(cbxPagamento, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtBuscaCliente)
                            .addComponent(txtCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 512, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 61, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(txtData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtBuscaCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(cbxPagamento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtItem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(btnOK)
                    .addComponent(txtBuscaItem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(txtQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnOK, txtItem});

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Itens do pedido", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

        tblItens.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tblItens.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "DESCRIÇÃO DO PRODUTO", "VALOR DE VENDA", "QUANTIDADE"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblItens.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblItensMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblItens);
        if (tblItens.getColumnModel().getColumnCount() > 0) {
            tblItens.getColumnModel().getColumn(1).setMinWidth(250);
            tblItens.getColumnModel().getColumn(1).setMaxWidth(250);
            tblItens.getColumnModel().getColumn(2).setMinWidth(100);
            tblItens.getColumnModel().getColumn(2).setMaxWidth(100);
        }

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("<html><b>TOTAL DO PEDIDO:</b> R$</html>");

        txtValorTotal.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txtValorTotal.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 743, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtValorTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtValorTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5))
        );

        javax.swing.GroupLayout jPListaFornecedorLayout = new javax.swing.GroupLayout(jPListaFornecedor);
        jPListaFornecedor.setLayout(jPListaFornecedorLayout);
        jPListaFornecedorLayout.setHorizontalGroup(
            jPListaFornecedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPListaFornecedorLayout.setVerticalGroup(
            jPListaFornecedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPListaFornecedorLayout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jTabbedPane1.addTab("Pedido", new javax.swing.ImageIcon(getClass().getResource("/imagens/pedido.png")), jPListaFornecedor); // NOI18N

        jPanelNavegacao.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Ações", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

        btnNovo.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnNovo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/adicionar.png"))); // NOI18N
        btnNovo.setText("NOVO");
        btnNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovoActionPerformed(evt);
            }
        });

        btnSalvar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/salvar.png"))); // NOI18N
        btnSalvar.setText("SALVAR");
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        btnFechar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnFechar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/sair.png"))); // NOI18N
        btnFechar.setText("FECHAR");
        btnFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFecharActionPerformed(evt);
            }
        });

        btnCancelar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/cancelar.png"))); // NOI18N
        btnCancelar.setText("CANCELAR");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnLimpar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnLimpar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/limpar.png"))); // NOI18N
        btnLimpar.setText("LIMPAR");
        btnLimpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimparActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelNavegacaoLayout = new javax.swing.GroupLayout(jPanelNavegacao);
        jPanelNavegacao.setLayout(jPanelNavegacaoLayout);
        jPanelNavegacaoLayout.setHorizontalGroup(
            jPanelNavegacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelNavegacaoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnNovo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSalvar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCancelar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnLimpar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnFechar)
                .addContainerGap())
        );

        jPanelNavegacaoLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnCancelar, btnFechar, btnLimpar, btnNovo, btnSalvar});

        jPanelNavegacaoLayout.setVerticalGroup(
            jPanelNavegacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelNavegacaoLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanelNavegacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnNovo)
                    .addComponent(btnSalvar)
                    .addComponent(btnFechar)
                    .addComponent(btnCancelar)
                    .addComponent(btnLimpar))
                .addContainerGap(24, Short.MAX_VALUE))
        );

        jPanelNavegacaoLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnCancelar, btnFechar, btnLimpar, btnNovo, btnSalvar});

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanelNavegacao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTabbedPane1))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                .addComponent(jPanelNavegacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFecharActionPerformed
        // TODO add your handling code here:
        limpaCampos();
        dispose();
    }//GEN-LAST:event_btnFecharActionPerformed

    private void btnNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovoActionPerformed
        // TODO add your handling code here:
        desabelitaCamposBotoes(true);
        txtBuscaCliente.requestFocus();
    }//GEN-LAST:event_btnNovoActionPerformed

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        // TODO add your handling code here:
        if (validaCampos()) { //SE TIVER TUDO OK C/ OS CAMPOS (VALIDAÇÕES)
            Pedido pedido = new Pedido();
            pedido.setTotalPedido(total);
            pedido.setDataEmissao(Util.stringParaLocalDate(txtData.getText()));
            pedido.setCliente(clienteDAO.localizar(txtCliente.getText()));
            pedido.setPagamento(pagamentoDAO.localizar(cbxPagamento.getSelectedItem().toString()));
            mostraMensagens(pedidoDAO.salvar(pedido));
            listaAuxiliarItens.forEach((venda) -> {
                vendaDAO.incluir(venda);
            });
            limpaCampos();
            desabelitaCamposBotoes(false);
        }
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void txtBuscaClienteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscaClienteKeyReleased
        // TODO add your handling code here:        
        txtCliente.setText(clienteDAO.buscaCliente(txtBuscaCliente.getText()));
    }//GEN-LAST:event_txtBuscaClienteKeyReleased

    private void txtBuscaItemKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscaItemKeyReleased
        // TODO add your handling code here:
        produto = produtoDAO.buscaItem(txtBuscaItem.getText());
        if (produto == null) {
            txtItem.setText("");
        } else {
            txtItem.setText(produto.getDescricao());
        }
    }//GEN-LAST:event_txtBuscaItemKeyReleased

    private void btnOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOKActionPerformed
        // TODO add your handling code here:   
        if (validaBotaoOK()) {
            if (produto.getEstoque() < 10) {
                mostraMensagens("<html>Produto com pouca quantidade no estoque: <b>"
                        + produto.getEstoque() + "</b>!</html>");
                txtBuscaItem.requestFocus();
            } else {
                modelo.addRow(new Object[]{" " + produto.getDescricao(),
                    String.format(" %.2f", produto.getValorVenda()),
                    " " + txtQuantidade.getText()});
                quantidade = Integer.parseInt(txtQuantidade.getText());
                Venda venda = new Venda();
                Pedido pedido = new Pedido();
                pedido.setPedido_id(Integer.parseInt(txtId.getText()));
                venda.setPedido(pedido);
                venda.setProduto(produtoDAO.localizar(produto.getDescricao()));
                venda.setQuantidade(quantidade);
                venda.setValorVenda(produto.getValorVenda());
                listaAuxiliarItens.add(venda);
                txtValorTotal.setText(String.format("%.2f",
                        calculaTotal((produto.getValorVenda() * quantidade))));
            }
        }
    }//GEN-LAST:event_btnOKActionPerformed

    private void btnLimparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimparActionPerformed
        // TODO add your handling code here:
        total = 0;
        limpaCampos();
        txtBuscaCliente.requestFocus();
    }//GEN-LAST:event_btnLimparActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        total = 0;
        limpaCampos();
        desabelitaCamposBotoes(false);
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void tblItensMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblItensMouseClicked
        // TODO add your handling code here:
        int linhaSelecionada = tblItens.getSelectedRow();
        String[] partes = modelo.getValueAt(linhaSelecionada, 1).toString().split(",");
        double valor = Double.parseDouble((partes[0] + "." + partes[1]));
        int quant = Integer.parseInt(modelo.getValueAt(linhaSelecionada, 2).toString().trim());
        int opcao = JOptionPane.showOptionDialog(this, "Confirmar a exclusão do"
                + " produto " + modelo.getValueAt(linhaSelecionada, 0).toString()
                + "?", "Message", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
                null, new String[]{"Sim", "Não"}, "Sim");
        if (opcao == 0) {
            modelo.removeRow(linhaSelecionada);
            listaAuxiliarItens.remove(linhaSelecionada);
            txtValorTotal.setText(String.format("%.2f", (total - (valor * quant))));
        }
    }//GEN-LAST:event_tblItensMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    //RETIREI ESSE TRECHO QUE DEIXAVA OS BTNS ARREDONDADOS                    
                    //javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    //E COLOQUEI ESSE Q DEIXA A APARENCIA DOS BTNS IGUAIS AO DO SO EM Q ESTA RODANDO O SOFTWARE
                    javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FormPedido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FormPedido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FormPedido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FormPedido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FormPedido form = new FormPedido(new javax.swing.JFrame(), true);
                form.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                form.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnFechar;
    private javax.swing.JButton btnLimpar;
    private javax.swing.JButton btnNovo;
    private javax.swing.JButton btnOK;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JComboBox<Pagamento> cbxPagamento;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPListaFornecedor;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanelNavegacao;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private java.util.List<Pagamento> listaPagamentos;
    private javax.swing.JTable tblItens;
    private javax.swing.JTextField txtBuscaCliente;
    private javax.swing.JTextField txtBuscaItem;
    private javax.swing.JTextField txtCliente;
    private javax.swing.JTextField txtData;
    private javax.swing.JTextField txtId;
    private javax.swing.JTextField txtItem;
    private javax.swing.JTextField txtQuantidade;
    private javax.swing.JLabel txtValorTotal;
    // End of variables declaration//GEN-END:variables
}
