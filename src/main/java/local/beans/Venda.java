package local.beans;

import java.beans.Transient;
import java.io.Serializable;

/**
 *
 * @author Rafael
 */
public class Venda implements Serializable {
    
    private Pedido pedido;
    private Produto produto;
    private Integer quantidade;
    private Double valorVenda;

    public Venda() {
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Double getValorVenda() {
        return valorVenda;
    }

    public void setValorVenda(Double valorVenda) {
        this.valorVenda = valorVenda;
    }
 
    /**
     * METO Q SERVE P/ MOSTRAR O ID DO PEDIDO FORMATADO NO FORM (TELA), A
     * ANOTACAO @Transient SERVE P/ ESSA FINALIDADE (SOMENTE MOSTRAR) E P/ DIZER
     * Q ESSA DADO N SERA PERCISTIDO NO BD
     *
     * @return O ID FORMATADO
     */
    @Transient
    public String getIdPedidoFormatado() {
        return String.format(" %d", pedido.getPedido_id());
    }
    
    /**
     * METO Q CRIEI P/ MOSTRAR O VALOR DE VENDA DO PRODUTO FORMATADO NO FORM
     * (TELA), A ANOTACAO @Transient SERVE P/ ESSA FINALIDADE (SOMENTE MOSTRAR)
     * E P/ DIZER Q ESSA DADO N SERA PERCISTIDO NO BD
     *
     * @return O VALOR DE VENDA FORMATADO
     */
    @Transient
    public String getValorVendaFormatada() {
        return String.format(" R$ %.2f", valorVenda);
    }
}
