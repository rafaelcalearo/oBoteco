package local.beans;

import java.beans.Transient;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

/**
 *
 * @author Rafael
 */
public class Compra implements Serializable {

    private Integer compra_id;
    private Fornecedor fornecedor;
    private Produto produto;
    private Double valorCompra;
    private Integer quantidade;
    private LocalDate dataCompra;
    private Double total;

    public Compra() {

    }

    public Integer getCompra_id() {
        return compra_id;
    }

    public void setCompra_id(Integer compra_id) {
        this.compra_id = compra_id;
    }

    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public Double getValorCompra() {
        return valorCompra;
    }

    public void setValorCompra(Double valorCompra) {
        this.valorCompra = valorCompra;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public LocalDate getDataCompra() {
        return dataCompra;
    }

    public void setDataCompra(LocalDate dataCompra) {
        this.dataCompra = dataCompra;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.compra_id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Compra other = (Compra) obj;
        if (!Objects.equals(this.compra_id, other.compra_id)) {
            return false;
        }
        return true;
    }

    /**
     * METO Q CRIEI P/ MOSTRAR O ID DA COMPRA FORMATADO NO FORM (TELA), A
     * ANOTACAO @Transient SERVE P/ ESSA FINALIDADE (SOMENTE MOSTRAR) E P/ DIZER
     * Q ESSA DADO N SERA PERCISTIDO NO BD
     *
     * @return O ID FORMATADO
     */
    @Transient
    public String getCompraIdFormatado() {
        return String.format(" %d", compra_id);
    }

    /**
     * METO Q CRIEI P/ MOSTRAR O VALOR DA COMPRA FORMATADO NO FORM (TELA), A
     * ANOTACAO @Transient SERVE P/ ESSA FINALIDADE (SOMENTE MOSTRAR) E P/ DIZER
     * Q ESSA DADO N SERA PERCISTIDO NO BD
     *
     * @return O VALOR DE COMPRA FORMATADO
     */
    @Transient
    public String getValorCompraFormatado() {
        return String.format(" R$ %.2f", valorCompra);
    }

    /**
     * METO Q CRIEI P/ MOSTRAR A QUANTIDADE DE PRODUTOS DA COMPRA FORMATADO NO
     * FORM (TELA), A ANOTACAO @Transient SERVE P/ ESSA FINALIDADE (SOMENTE
     * MOSTRAR) E P/ DIZER Q ESSA DADO N SERA PERCISTIDO NO BD
     *
     * @return A QUANTIDADE DE PRODUTOS FORMATADA
     */
    @Transient
    public String getQuantidadeFormatada() {
        return String.format(" %d", quantidade);
    }

    /**
     * Q MOSTRA A DATA DA COMPRA FORMATADA NO FORM (TELA), A ANOTACAO @Transient
     * SERVE P/ ESSA FINALIDADE (SOMENTE MOSTRAR) E DIZER Q ESSE DADO N SERA
     * PERSISTIDO NO BD. O METODO plusDays() APLICADO NA DATA SERVE P/ AUMENTAR
     * UM DIA A MESMA, POIS NAS CONVERSOES ANTERRIOS A DATA CHEGA ATE AQUI
     * FALTANDO UM DIA
     *
     * @return A DATA DA COMPRA DE PRODUTOS FORMATADA C/ UM DIA A MAIS
     */
    @Transient
    public String getDataCompraFormatada() {
        DateTimeFormatter formatador = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate data = dataCompra.plusDays(1);
        return data.format(formatador);
    }

    /**
     * METO Q CRIEI P/ MOSTRAR O TOTAL DA COMPRA DE PRODUTOS FORMATADO NO FORM
     * (TELA), A ANOTACAO @Transient SERVE P/ ESSA FINALIDADE (SOMENTE MOSTRAR)
     * E P/ DIZER Q ESSE DADO N SERA PERSISTIDO NO BD
     *
     * @return O TOTAL DE PRODUTOS COMPRADOS FORMATADO
     */
    @Transient
    public String getTotalFormatado() {
        return String.format(" R$ %.2f", total);
    }
}
