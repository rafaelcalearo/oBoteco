package local.beans;

import java.beans.Transient;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Rafael
 */
public class Produto implements Serializable {

    private Integer produto_id;
    private String descricao;
    private Double valorVenda;
    private Integer estoque; //QUANTIDADE
    private Integer ativo;

    public Produto() {
    }

    public Integer getProduto_id() {
        return produto_id;
    }

    public void setProduto_id(Integer produto_id) {
        this.produto_id = produto_id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValorVenda() {
        return valorVenda;
    }

    public void setValorVenda(Double valorVenda) {
        this.valorVenda = valorVenda;
    }

    public Integer getEstoque() {
        return estoque;
    }

    public void setEstoque(Integer estoque) {
        this.estoque = estoque;
    }

    public Integer getAtivo() {
        return ativo;
    }

    public void setAtivo(Integer ativo) {
        this.ativo = ativo;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.produto_id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Produto other = (Produto) obj;
        if (!Objects.equals(this.produto_id, other.produto_id)) {
            return false;
        }
        return true;
    }

    /**
     * METODO P MOSTRAR A DESCRICAO DO PRODUTO POSSIVEL DE LER POR HUMANOS
     *
     * @return A DESCRICAO DO PRODUTO
     */
    @Override
    public String toString() {
        return descricao;
    }

    /**
     * METO Q CRIEI P/ MOSTRAR O ID DO PRODUTO FORMATADO NO FORM (TELA), A
     * ANOTACAO @Transient SERVE P/ ESSA FINALIDADE (SOMENTE MOSTRAR) E P/ DIZER
     * Q ESSA DADO N SERA PERCISTIDO NO BD
     *
     * @return O ID FORMATADO
     */
    @Transient
    public String getProdutoIdFormatado() {
        return String.format(" %d", produto_id);
    }

    /**
     * METO Q CRIEI P/ MOSTRAR O VALOR DE VENDA DO PRODUTO FORMATADO NO FORM
     * (TELA), A ANOTACAO @Transient SERVE P/ ESSA FINALIDADE (SOMENTE MOSTRAR)
     * E P/ DIZER Q ESSA DADO N SERA PERCISTIDO NO BD
     *
     * @return O VALOR DE VENDA FORMATADO
     */
    @Transient
    public String getValorVendaFormatada() {
        return String.format(" R$ %.2f", valorVenda);
    }

    /**
     * METO Q CRIEI P/ MOSTRAR A QUANTIDADE DO ESTOQUE DO PRODUTO FORMATADO NO
     * FORM (TELA), A ANOTACAO @Transient SERVE P/ ESSA FINALIDADE (SOMENTE
     * MOSTRAR) E P/ DIZER Q ESSA DADO N SERA PERCISTIDO NO BD
     *
     * @return O VALOR DE VENDA FORMATADO
     */
    @Transient
    public String getEstoqueFormatado() {
        return String.format(" %d", estoque);
    }

}
