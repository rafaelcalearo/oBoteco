package local.beans;

import java.beans.Transient;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Rafael
 */
public class Funcionario extends Pessoa implements Serializable {

    private Integer funcionario_id;
    private String nome;
    private String CPF;
    private String senha;
    private String foto;

    public Funcionario() {
    }

    public Integer getFuncionario_id() {
        return funcionario_id;
    }

    public void setFuncionario_id(Integer funcionario_id) {
        this.funcionario_id = funcionario_id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCPF() {
        return CPF;
    }

    public void setCPF(String CPF) {
        this.CPF = CPF;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getCelular() {
        return celular;
    }

    @Override
    public void setCelular(String celular) {
        this.celular = celular;
    }

    @Override
    public Integer getAtivo() {
        return ativo;
    }

    @Override
    public void setAtivo(Integer ativo) {
        this.ativo = ativo;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.funcionario_id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Funcionario other = (Funcionario) obj;
        if (!Objects.equals(this.funcionario_id, other.funcionario_id)) {
            return false;
        }
        return true;
    }

    /**
     * METODO P MOSTRAR O NOME DO FUNCIONARIO POSSIVEL DE LER POR HUMANOS
     *
     * @return O NOME DO FUNCIONARIO
     */
    @Override
    public String toString() {
        return nome;
    }

    /**
     * METO Q CRIEI P/ MOSTRAR O ID DO FUNCIONARIO FORMATADO NO FORM (TELA), A
     * ANOTACAO @Transient SERVE P/ ESSA FINALIDADE (SOMENTE MOSTRAR) E P/ DIZER
     * Q ESSA DADO N SERA PERCISTIDO NO BD
     *
     * @return O ID FORMATADO
     */
    @Transient
    public String getFuncionarioIdFormatado() {
        return String.format(" %d", funcionario_id);
    }
}
