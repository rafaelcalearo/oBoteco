package local.beans;

import java.beans.Transient;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Rafael
 */
public class Pagamento implements Serializable {

    private Integer pagamento_id;
    private String descricao;
    private Integer ativo;

    public Pagamento() {
    }

    public Integer getPagamento_id() {
        return pagamento_id;
    }

    public void setPagamento_id(Integer pagamento_id) {
        this.pagamento_id = pagamento_id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getAtivo() {
        return ativo;
    }

    public void setAtivo(Integer ativo) {
        this.ativo = ativo;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.pagamento_id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pagamento other = (Pagamento) obj;
        if (!Objects.equals(this.pagamento_id, other.pagamento_id)) {
            return false;
        }
        return true;
    }

    /**
     * METODO PARA MOSTRAR A DESCRICAO DA FORMA DE PAGAMENTO POSSIVEL DE LER POR
     * HUMANOS
     *
     * @return A DESCRICAO DA FORMA DE PAGAMENTO
     */
    @Override
    public String toString() {
        return descricao;
    }

    /**
     * METO Q CRIEI P/ MOSTRAR O ID DO PAGAMENTO FORMATADO NO FORM (TELA), A
     * ANOTACAO @Transient SERVE P/ ESSA FINALIDADE (SOMENTE MOSTRAR) E P/ DIZER
     * Q ESSA DADO N SERA PERCISTIDO NO BD
     *
     * @return O ID FORMATADO
     */
    @Transient
    public String getPagamentoIdFormatado() {
        return String.format(" %d", pagamento_id);
    }

}
