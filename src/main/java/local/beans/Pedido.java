package local.beans;

import java.beans.Transient;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

/**
 *
 * @author Rafael
 */
public class Pedido implements Serializable {

    private Integer pedido_id;
    private Double totalPedido;
    private LocalDate dataEmissao;
    private Cliente cliente;
    private Pagamento pagamento;

    public Pedido() {
    }

    public Integer getPedido_id() {
        return pedido_id;
    }

    public void setPedido_id(Integer pedido_id) {
        this.pedido_id = pedido_id;
    }

    public Double getTotalPedido() {
        return totalPedido;
    }

    public void setTotalPedido(Double totalPedido) {
        this.totalPedido = totalPedido;
    }

    public LocalDate getDataEmissao() {
        return dataEmissao;
    }

    public void setDataEmissao(LocalDate dataEmissao) {
        this.dataEmissao = dataEmissao;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Pagamento getPagamento() {
        return pagamento;
    }

    public void setPagamento(Pagamento pagamento) {
        this.pagamento = pagamento;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.pedido_id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pedido other = (Pedido) obj;
        if (!Objects.equals(this.pedido_id, other.pedido_id)) {
            return false;
        }
        return true;
    }
    
    /**
     * METO Q SERVE P/ MOSTRAR O ID DO PEDIDO FORMATADO NO FORM (TELA), A
     * ANOTACAO @Transient SERVE P/ ESSA FINALIDADE (SOMENTE MOSTRAR) E P/ DIZER
     * Q ESSA DADO N SERA PERCISTIDO NO BD
     *
     * @return O ID FORMATADO
     */
    @Transient
    public String getIdPedidoFormatado() {
        return String.format(" %d", pedido_id);
    }

    /**
     * METO Q CRIEI P/ MOSTRAR O VALOR DA COMPRA FORMATADO NO FORM (TELA), A
     * ANOTACAO @Transient SERVE P/ ESSA FINALIDADE (SOMENTE MOSTRAR) E P/ DIZER
     * Q ESSA DADO N SERA PERCISTIDO NO BD
     *
     * @return O VALOR DE COMPRA FORMATADO
     */
    @Transient
    public String getTotalPedidoFormatado() {
        return String.format(" R$ %.2f", totalPedido);
    }
    
    /**
     * Q MOSTRA A DATA DA COMPRA FORMATADA NO FORM (TELA), A ANOTACAO @Transient
     * SERVE P/ ESSA FINALIDADE (SOMENTE MOSTRAR) E DIZER Q ESSE DADO N SERA
     * PERSISTIDO NO BD. O METODO plusDays() APLICADO NA DATA SERVE P/ AUMENTAR
     * UM DIA A MESMA, POIS NAS CONVERSOES ANTERRIOS A DATA CHEGA ATE AQUI
     * FALTANDO UM DIA
     *
     * @return A DATA DA COMPRA DE PRODUTOS FORMATADA C/ UM DIA A MAIS
     */
    @Transient
    public String getDataEmissaoFormatada() {
        DateTimeFormatter formatador = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate data = dataEmissao.plusDays(1);
        return " " + data.format(formatador);
    }
}
