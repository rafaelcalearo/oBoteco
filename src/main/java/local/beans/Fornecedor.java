package local.beans;

import java.beans.Transient;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Rafael
 */
public class Fornecedor extends Pessoa implements Serializable {

    private Integer fornecedor_id;
    private String razaoSocial;
    private String CNPJ;
    private String UF;

    public Fornecedor() {
    }

    public Integer getFornecedor_id() {
        return fornecedor_id;
    }

    public void setFornecedor_id(Integer fornecedor_id) {
        this.fornecedor_id = fornecedor_id;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public String getCNPJ() {
        return CNPJ;
    }

    public void setCNPJ(String CNPJ) {
        this.CNPJ = CNPJ;
    }

    public String getUF() {
        return UF;
    }

    public void setUF(String UF) {
        this.UF = UF;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getCelular() {
        return celular;
    }

    @Override
    public void setCelular(String celular) {
        this.celular = celular;
    }

    @Override
    public Integer getAtivo() {
        return ativo;
    }

    @Override
    public void setAtivo(Integer ativo) {
        this.ativo = ativo;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.fornecedor_id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Fornecedor other = (Fornecedor) obj;
        if (!Objects.equals(this.fornecedor_id, other.fornecedor_id)) {
            return false;
        }
        return true;
    }

    /**
     * METODO P MOSTRAR A RAZAO SOCIAL DO FORNECEDOR POSSIVEL DE LER POR HUMANOS
     *
     * @return A RAZAO SOCIAL DO FORNECEDOR
     */
    @Override
    public String toString() {
        return razaoSocial;
    }

    /**
     * METO Q CRIEI P/ MOSTRAR O ID DO FORNECEDOR FORMATADO NO FORM (TELA), A
     * ANOTACAO @Transient SERVE P/ ESSA FINALIDADE (SOMENTE MOSTRAR) E P/ DIZER
     * Q ESSA DADO N SERA PERCISTIDO NO BD
     *
     * @return O ID FORMATADO
     */
    @Transient
    public String getFornecedorIdFormatado() {
        return String.format(" %d", fornecedor_id);
    }
}
