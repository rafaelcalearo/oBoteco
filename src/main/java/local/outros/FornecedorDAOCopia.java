package local.outros;

import local.DAO.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import local.beans.Fornecedor;
import local.utilitarios.Conexao;
//import local.outros.Dados;

/**
 *
 * @author Rafael
 */
public class FornecedorDAOCopia {

    /**
     * USANDO A CLASSE DADOS
     *
     * ESSE METODO PEGA UMA LISTA DE OBJETOS DO TIPO FORNECEDOR DA BASE DE DADOS
     * DO TIPO LISTA QUE ESTA NA CLASSE DADOS (ENCONTRADA NO PACOTE:
     * local.outros.Dados;), COM ISSO, QUER DIZER Q OS DADOS Ñ SÃO SALVOS EM UM
     * BD E SIM NESSA CLASSE Q APOS FECHAR O PROGRAMA ESSES DADOS SÃO PERDIDOS!
     *
     * public List<Fornecedor> getLista() { return Dados.listaFornecedores; }
     *
     * return É UM LISTA DO TIPO FORNECEDOR
     *
     * USANDO O BANCO DE DADOS
     *
     * @return É UMA LISTA DE OBJETOS DO TIPO FORNECEDOR
     */
    public List<Fornecedor> getLista() {
        String sql = "SELECT * FROM fornecedor WHERE ativo = 1";
        List<Fornecedor> lista = new ArrayList<>();
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Fornecedor fornecedor = new Fornecedor();
                fornecedor.setFornecedor_id(rs.getInt("fornecedor_id"));
                fornecedor.setRazaoSocial(rs.getString("razaoSocial"));
                fornecedor.setCNPJ(rs.getString("cnpj"));
                fornecedor.setUF(rs.getString("UF"));
                fornecedor.setEmail(rs.getString("email"));
                fornecedor.setCelular(rs.getString("celular"));
                lista.add(fornecedor);
            }
            pst.close();
            rs.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
        }
        return lista;
    }

    public List<Fornecedor> getListaDesativados() {
        String sql = "SELECT * FROM fornecedor WHERE ativo = 0";
        List<Fornecedor> lista = new ArrayList<>();
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Fornecedor fornecedor = new Fornecedor();
                fornecedor.setFornecedor_id(rs.getInt("fornecedor_id"));
                fornecedor.setRazaoSocial(rs.getString("razaoSocial"));
                fornecedor.setCNPJ(rs.getString("cnpj"));
                fornecedor.setUF(rs.getString("UF"));
                fornecedor.setEmail(rs.getString("email"));
                fornecedor.setCelular(rs.getString("celular"));
                lista.add(fornecedor);
            }
            pst.close();
            rs.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
        }
        return lista;
    }

    /**
     * USANDO A CLASSE DADOS
     *
     * ESSE METODO ADICIONAVA NA CLASSE DADOS (ENCONTRADA NO PACOTE:
     * local.outros.Dados;) UM REGISTRO CASO O OBJETO ESTIVESSE C/ O ID
     * VAZIO(NULO), CASO CONTRARIO Ñ FAZIA NADA
     *
     * public boolean salvar(Fornecedor obj) { if (obj.getFornecedor_id() ==
     * null) { Integer fornecedorId = Dados.listaFornecedores.size() + 1;
     * obj.setFornecedor_id(fornecedorId); Dados.listaFornecedores.add(obj); }
     * return true; }
     *
     * USANDO O BANCO DE DADOS
     *
     * @param fornecedor É UM OBJ DO TIPO FORNECEDOR P/ DEPOIS SALVAR/ALTERAR
     * @return ANTES ERA BOOLEAN AI TROQUEI POIS OS JOPTIONPANE Ñ QUERIAM FUNFA,
     * MAS RETORNA UM N° INTEIRO INFORMANDO S DEU CERTO OU Ñ
     */
    public int salvar(Fornecedor fornecedor) {
        if (fornecedor.getFornecedor_id() == null) {
            return incluir(fornecedor);
        } else {
            return alterar(fornecedor);
        }
    }

    /**
     *
     * @param fornecedor
     * @return
     */
    public int incluir(Fornecedor fornecedor) {
        String sql = "INSERT INTO fornecedor (razaoSocial, cnpj, UF, email, "
                + "celular, ativo) VALUES(?,?,?,?,?,?)";
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            pst.setString(1, fornecedor.getRazaoSocial());
            pst.setString(2, fornecedor.getCNPJ());
            pst.setString(3, fornecedor.getUF());
            pst.setString(4, fornecedor.getEmail());
            pst.setString(5, fornecedor.getCelular());
            pst.setInt(6, fornecedor.getAtivo());
            if (pst.executeUpdate() > 0) {
                pst.close();
                return 1;
            } else {
                pst.close();
                return 2;
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
            return 0;
        }
    }

    public int alterar(Fornecedor fornecedor) {
        String sql = "UPDATE fornecedor SET razaoSocial = ?, cnpj = ?, UF = ?, "
                + "email = ?, celular = ?, ativo = ? WHERE fornecedor_id = ?";
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            pst.setString(1, fornecedor.getRazaoSocial());
            pst.setString(2, fornecedor.getCNPJ());
            pst.setString(3, fornecedor.getUF());
            pst.setString(4, fornecedor.getEmail());
            pst.setString(5, fornecedor.getCelular());
            pst.setInt(6, fornecedor.getAtivo());
            pst.setInt(7, fornecedor.getFornecedor_id());
            if (pst.executeUpdate() > 0) {
                pst.close();
                return 3;
            } else {
                pst.close();
                return 4;
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
            return 0;
        }
    }

    public Fornecedor localizar(Integer id) {
        String sql = "SELECT * FROM fornecedor WHERE fornecedor_id = ?";
        Fornecedor fornecedor = new Fornecedor();
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            pst.setInt(1, id);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                fornecedor.setFornecedor_id(rs.getInt("fornecedor_id"));
                fornecedor.setRazaoSocial(rs.getString("razaoSocial"));
                fornecedor.setCNPJ(rs.getString("cnpj"));
                fornecedor.setUF(rs.getString("UF"));
                fornecedor.setEmail(rs.getString("email"));
                fornecedor.setCelular(rs.getString("celular"));
                return fornecedor;
            }            
            pst.close();
            rs.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
        }
        return null;
    }
}
