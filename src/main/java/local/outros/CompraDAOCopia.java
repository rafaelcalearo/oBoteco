package local.outros;

import local.DAO.*;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import local.beans.Compra;
import local.utilitarios.Conexao;

/**
 *
 * @author Rafael
 */
public class CompraDAOCopia {

    //VARIAVEIS DE INSTANCIA
    FornecedorDAO fornecedorDAO;
    ProdutoDAO produtoDAO;

    //CONSTRUTOR 
    public CompraDAOCopia() {
        fornecedorDAO = new FornecedorDAO();
        produtoDAO = new ProdutoDAO();
    }

    /**
     * METODO Q LISTA AS COMPRAS CADASTRADAS NO BANCO DE DADOS
     * @return UMA LISTA DESSAS COMPRAS P/ SEREM MOSTRADAS NA TELA
     */
    public List<Compra> getLista() {
        String sql = "SELECT * FROM compraproduto";
        List<Compra> lista = new ArrayList<>();
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Compra compra = new Compra();
                compra.setCompra_id(rs.getInt("compra_id"));
                compra.setFornecedor(fornecedorDAO.localizar(rs.getInt("fornecedor_id")));
                compra.setProduto(produtoDAO.localizar(rs.getInt("produto_id")));
                compra.setValorCompra(rs.getDouble("valorCompra"));
                compra.setQuantidade(rs.getInt("quantidade"));
                /**
                 * AQUI É CONVERTIDO A DATA Q VEM DO BD DO TIPO "DATE" P/ O TIPO
                 * "LOCALDATE" ATRAVES DO METODO dateParaLocalDate() Q ESTA NA
                 * CLASSE UTIL DO PACOTE UTILITÁRIOS, CASO N QUISESSE USAR A
                 * CLASSE UTIL PODERIA FAZER DIRETO ASSIM:
                 *
                 * compra.setDataCompra(new
                 * java.sql.Date(rs.getDate("dataCompra").getTime()).toLocalDate());
                 *
                 * OU ASSIM:
                 * compra.setDataCompra(Util.dateSQLParaLocalDate(rs.getDate("dataCompra")));
                 */
                compra.setDataCompra(rs.getDate("dataCompra").toLocalDate());
                compra.setTotal(rs.getDouble("total"));
                lista.add(compra);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
        }
        return lista;
    }

    public String salvar(Compra compra) {
        if (compra.getCompra_id() == null) {
            return incluir(compra);
        } else {
            return alterar(compra);
        }
    }

    /**
     *
     * @param compra
     * @return
     */
    public String incluir(Compra compra) {
        String sql = "INSERT INTO compraproduto (fornecedor_id, produto_id, "
                + "valorCompra, quantidade, dataCompra, total)"
                + " VALUES(?,?,?,?,?,?)";
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            pst.setInt(1, compra.getFornecedor().getFornecedor_id());
            pst.setInt(2, compra.getProduto().getProduto_id());
            pst.setDouble(3, compra.getValorCompra());
            pst.setInt(4, compra.getQuantidade());
            pst.setDate(5, Date.valueOf(compra.getDataCompra()));
            pst.setDouble(6, compra.getTotal());
            if (pst.executeUpdate() > 0
                    && produtoDAO.atualizaProduto(compra.getProduto().getProduto_id(),
                            compra.getValorCompra(),
                            compra.getQuantidade())) {
                return "Compra dos produtos realizada com sucesso!";
            } else {
                return "Compra dos produtos não realizada com sucesso!";
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
            return "";
        }
    }

    public String alterar(Compra compra) {
        String sql = "UPDATE compraproduto SET fornecedor_id = ?, produto_id = ?, "
                + "valorCompra = ?, quantidade = ?, dataCompra = ?, total = ? WHERE compra_id = ?";
        try {
            PreparedStatement pst = new Conexao().getPreparedStatement(sql);
            pst.setInt(1, compra.getFornecedor().getFornecedor_id());
            pst.setInt(2, compra.getProduto().getProduto_id());
            pst.setDouble(3, compra.getValorCompra());
            pst.setInt(4, compra.getQuantidade());
            pst.setDate(5, Date.valueOf(compra.getDataCompra()));
            pst.setDouble(6, compra.getTotal());
            pst.setInt(7, compra.getCompra_id());
            if (pst.executeUpdate() > 0
                    && produtoDAO.atualizaProduto(compra.getProduto().getProduto_id(),
                            compra.getValorCompra(), compra.getQuantidade())) {
                pst.close();
                return "Alteração dos dados da compra realizados com sucesso!";
            } else {
                pst.close();
                return "Alteração dos dados da compra não realizados com sucesso!";
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro de SQL: " + e.getMessage());
            return "";
        }
    }
}
