# O Boteco

O Boteco é um trabalho que faz parte da avaliação da disciplina de Algoritmos e Programação II do segundo 
semestre da Faculdade de Tecnologia Senac Pelotas–RS. Desenvolvido em **Java** pelo grupo composto por [Igor Silva](https://gitlab.com/Maartyr), [Thauã Borges](https://gitlab.com/thaua97), 
[Lussandro Marques](https://gitlab.com/lmarques) e [Rafael Calearo](https://gitlab.com/rafaelcalearo) (criador do repositório) . 

## Objetivo

O objetivo do  produto é facilitar o processo de gerenciamento de vendas para estabelicimentos com foco em bebidas e petiscos. Atualmente o software conta com as funções de Controle de estoque, Gerenciamento de usuarios e Controle de pedidos.

## Instalação.

A Instalação do software pode ser efetuada atraves do git ou com o download do arquivo. 

 - [.zip](https://gitlab.com/rafaelcalearo/projetoBoteco/repository/master/archive.zip) 
 - [.tar.gz](https://gitlab.com/rafaelcalearo/projetoBoteco/repository/master/archive.tar.gz)
 - [.tar.bz2](https://gitlab.com/rafaelcalearo/projetoBoteco/repository/master/archive.tar.bz2)
 - [.tar](https://gitlab.com/rafaelcalearo/projetoBoteco/repository/master/archive.tar)


Atraves do git devemos efetuar os seguintes passos...

1. Para efetuar a instalação devemos utilizar a ferramenta git atraves do prompt de comando. Caso você esteja utilizando a distribuição Windows, você deve efetuar a instalação da ferramenta.
[Link para download.](https://git-scm.com/download/win)


2. Com o prompt abarto devemos efetuar o clone do projeto.

 SSH
 ```bash
 $ git clone git@gitlab.com:rafaelcalearo/oBoteco.git
```

https
```bash
$ git clone https://gitlab.com/rafaelcalearo/oBoteco.git
```